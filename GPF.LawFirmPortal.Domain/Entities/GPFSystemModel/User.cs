﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class User
    {
        public User()
        {
            InverseCreatedByNavigation = new HashSet<User>();
            InverseModifiedByNavigation = new HashSet<User>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool? Active { get; set; }
        public string DisplayName { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public short? RecordStatus { get; set; }
        public bool? IsAdmin { get; set; }
        public bool? AutoAdjustUac { get; set; }
        public bool? CanMakeAdminChanges { get; set; }
        public bool? ComplianceView { get; set; }
        public bool? CanViewDocAudit { get; set; }
        public bool? CanExportAllDocs { get; set; }
        public string SpFilter { get; set; }
        public bool? IsReadOnly { get; set; }
        public bool? LegalUser { get; set; }
        public bool? SpUser1 { get; set; }
        public bool? CanMarkPosent { get; set; }
        public bool? CanGenPo { get; set; }
        public bool? CanViewRiskMgt { get; set; }
        public string Computername { get; set; }
        public string Appversion { get; set; }
        public DateTime? Dateloggedin { get; set; }
        public string LegalViewRoles { get; set; }

        public virtual User CreatedByNavigation { get; set; }
        public virtual User ModifiedByNavigation { get; set; }
        public virtual ICollection<User> InverseCreatedByNavigation { get; set; }
        public virtual ICollection<User> InverseModifiedByNavigation { get; set; }
    }
}
