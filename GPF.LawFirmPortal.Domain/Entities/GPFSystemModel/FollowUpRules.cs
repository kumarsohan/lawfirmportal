﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class FollowUpRules
    {
        public int FollowUpId { get; set; }
        public int? CompanyId { get; set; }
        public int? StatusId { get; set; }
        public int? SubStatusId { get; set; }
        public decimal? CappedAmountMin { get; set; }
        public decimal? CappedAmountMax { get; set; }
        public int? MaxTime { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public short? RecordStatus { get; set; }
    }
}
