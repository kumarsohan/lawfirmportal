﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class VwAmountFundedDetail
    {
        public int? CaseId { get; set; }
        public string ClientTypeName { get; set; }
        public DateTime? CheckDate { get; set; }
        public decimal? Amount { get; set; }
        public string ClientType { get; set; }
        public string DisbursementType { get; set; }
    }
}
