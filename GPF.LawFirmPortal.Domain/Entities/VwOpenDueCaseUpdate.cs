﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class VwOpenDueCaseUpdate
    {
        public int PortfolioId { get; set; }
        public int? CaseId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Note { get; set; }
        public int? CasePhaseId { get; set; }
        public int? CaseStatusReasonId { get; set; }
        public string LfcaseDetailsName { get; set; }
        public string CombinedName { get; set; }
        public string LawFirmName { get; set; }
        public int LawFirmId { get; set; }
        public decimal ContractCv { get; set; }
        public decimal AmountOwed { get; set; }
        public DateTime? Dol { get; set; }
        public string CaseStatus { get; set; }
        public int? CaseStatusId { get; set; }
        public int? CaseStatusReasonIdold { get; set; }
        public DateTime? NextFollowUpDate { get; set; }
        public long? RowNumberRank { get; set; }
        public string Reason { get; set; }
        public DateTime? StatusDateEntered { get; set; }
        public DateTime? Dob { get; set; }
        public string Status { get; set; }
        public int? NoOfDaysLeft { get; set; }
        public decimal ContractPrincipal { get; set; }
        public DateTime? DateFunded { get; set; }
        public string CasePhase { get; set; }
        public int? CasePhaseIdold { get; set; }
        public decimal ApprovalAmount { get; set; }
    }
}
