﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class VwLawFirm
    {
        public int LawFirmId { get; set; }
        public string Name { get; set; }
        public bool? IsSearchCompleted { get; set; }
        public bool? IsBlackListed { get; set; }
    }
}
