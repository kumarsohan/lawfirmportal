﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class CaseUpdates
    {
        public int Id { get; set; }
        public int? PortfolioId { get; set; }
        public int? CaseId { get; set; }
        public int? CasePhaseId { get; set; }
        public int? CaseStatusDetailId { get; set; }
        public string Comment { get; set; }
        public bool? IsAttorneyComment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? RecordStatus { get; set; }
    }
}
