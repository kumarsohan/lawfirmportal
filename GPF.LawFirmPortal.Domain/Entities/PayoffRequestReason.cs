﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class PayoffRequestReason
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public bool? RecordStatus { get; set; }
    }
}
