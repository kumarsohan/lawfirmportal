﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class EmailTemplate
    {
        public int EmailTemplateId { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public bool? IsFilter { get; set; }
        public string Message { get; set; }
        public int? Display { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Pdfname { get; set; }
        public string ImageName { get; set; }
    }
}
