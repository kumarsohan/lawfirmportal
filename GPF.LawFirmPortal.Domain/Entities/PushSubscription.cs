﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class PushSubscription
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Endpoint { get; set; }
        public double? ExpirationTime { get; set; }
        public string P256dh { get; set; }
        public string Auth { get; set; }
    }
}
