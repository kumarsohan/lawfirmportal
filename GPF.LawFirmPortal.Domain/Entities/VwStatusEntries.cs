﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class VwStatusEntries
    {
        public int Id { get; set; }
        public DateTime? Dateentered { get; set; }
        public string Enteredby { get; set; }
        public string CombinedName { get; set; }
        public int? PortfolioId { get; set; }
        public int? Firmhistoryid { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdateReceived { get; set; }
        public int? CaseId { get; set; }
        public int? CaseStatusId { get; set; }
        public int? CaseContactStatusId { get; set; }
        public int? CaseStatusReasonId { get; set; }
        public string StatusCategory { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public bool? NoStatusChange { get; set; }
        public DateTime? FollowupDate { get; set; }
        public bool? InLegal { get; set; }
        public string Note { get; set; }
        public bool? ContactAttempted { get; set; }
        public string ContactPerson { get; set; }
        public string Called { get; set; }
        public string Emailed { get; set; }
        public string Faxed { get; set; }
        public bool? MailCorrespondance { get; set; }
        public bool? ECourts { get; set; }
        public bool? InPerson { get; set; }
        public bool? PoReq { get; set; }
        public DateTime? PayoffDate1 { get; set; }
        public decimal? PayoffAmt1 { get; set; }
        public DateTime? PayoffDate2 { get; set; }
        public decimal? PayoffAmt2 { get; set; }
        public string PoType { get; set; }
        public string PoPriority { get; set; }
        public string PoRequestedBy { get; set; }
        public string PoContactType { get; set; }
        public int? PoDaysOut { get; set; }
        public bool? ReductionReq { get; set; }
        public decimal? SettledFor { get; set; }
        public decimal? Liens { get; set; }
        public DateTime? EstPaymentDate { get; set; }
        public decimal? SuggestedAmt { get; set; }
        public decimal? ClientNet { get; set; }
        public DateTime? RedApprovalDate { get; set; }
        public decimal? RedApprovedAmt { get; set; }
        public bool? NoPortfolioId { get; set; }
        public bool? FromPyrus { get; set; }
        public string Source { get; set; }
        public int? CasePhaseId { get; set; }
        public decimal? AttorneyFee { get; set; }
        public decimal? MedicalLiens { get; set; }
        public DateTime? SettledForDate { get; set; }
        public string PoRequestersName { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? RedReqNegotiatedById { get; set; }
        public int? RedReqApprovedById { get; set; }
        public int? ReductionsNoteId { get; set; }
        public string SubpoenaInfo { get; set; }
        public int? SellingReasonId { get; set; }
    }
}
