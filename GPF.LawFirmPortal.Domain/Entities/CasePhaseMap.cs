﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class CasePhaseMap
    {
        public int LfcasePhaseId { get; set; }
        public string LfcasePhaseName { get; set; }
        public bool? IsDropdown { get; set; }
        public short? RecordStatus { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
