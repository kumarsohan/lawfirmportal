﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class EmailTriggerMapping
    {
        public int Id { get; set; }
        public int CaseStatusDetailId { get; set; }
        public bool? IsRiskMngmntEmailTrigger { get; set; }
        public string EmailMember { get; set; }
        public string DepartmentName { get; set; }
        public bool? RecordStatus { get; set; }

        public virtual CaseStatusDetailsMap CaseStatusDetail { get; set; }
    }
}
