﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class RequestPayOffLog
    {
        public int LogId { get; set; }
        public int? CaseId { get; set; }
        public int? PortfolioId { get; set; }
        public string Message { get; set; }
        public bool? IsRequestPayoff { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? PayoffRequestReasonId { get; set; }
    }
}
