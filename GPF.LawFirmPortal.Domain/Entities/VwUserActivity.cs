﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class VwUserActivity
    {
        public string UserName { get; set; }
        public string MachineName { get; set; }
        public string ActionName { get; set; }
        public DateTime? VisitDate { get; set; }
    }
}
