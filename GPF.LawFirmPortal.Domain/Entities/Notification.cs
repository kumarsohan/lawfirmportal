﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class Notification
    {
        public int NotificationId { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }
        public int? LawFirmId { get; set; }
        public string UserGroupName { get; set; }
        public string Message { get; set; }
        public bool? NotificationState { get; set; }
        public DateTime? ReadDate { get; set; }
    }
}
