﻿using System;
using System.Collections.Generic;

namespace GPF.LawFirmPortal.Domain.Entities
{
    public partial class EventAudit
    {
        public long EventId { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public string JsonData { get; set; }
        public string EventType { get; set; }
        public string UserName { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
