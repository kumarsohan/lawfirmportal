﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.Common.Models
{
    public class GPFMenusDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public int? SequenceNumber { get; set; }

        public List<GPFMenusDto> SubMenus { get; set; }
    }
}
