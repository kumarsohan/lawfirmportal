﻿namespace GPF.LawFirmPortal.Application.Common.Models
{
    public class DropDownViewModel
    {
        public int Value { get; set; }

        public string Text { get; set; }
    }
}
