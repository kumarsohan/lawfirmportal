﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.Common.Models
{
    public class LdapUser
    {
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string LawFirmID { get; set; }
        public string GroupName { get; set; }
    }
}
