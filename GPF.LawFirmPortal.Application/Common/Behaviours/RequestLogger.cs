﻿using System.Threading;
using System.Threading.Tasks;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace GPF.LawFirmPortal.Application.Common.Behaviours
{
    public class RequestLogger<TRequest> : IRequestPreProcessor<TRequest>
    {
        private readonly ILoggerService _logger;

        public RequestLogger(ILoggerService logger)
        {
            _logger = logger;
        }

        public async Task Process(TRequest request, CancellationToken cancellationToken)
        {
            var requestName = typeof(TRequest).Name;
           // _logger.LogInfo(string.Empty, string.Empty, string.Empty, $"Request: {requestName} {request}");
        }
    }
}
