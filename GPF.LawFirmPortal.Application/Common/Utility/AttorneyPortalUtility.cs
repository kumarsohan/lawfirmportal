﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Security.Claims;
using GPF.LawFirmPortal.Application.Common.Models;
using Newtonsoft.Json;

namespace GPF.LawFirmPortal.Application.Common.Utility
{
    public static class AttorneyPortalUtility
    {
        public static bool SetPropertyValue<T>(T type, PropertyInfo[] propertyInfoArray, string newValue, string propertyName)
        {
            bool isPropertySet = false;
            try
            {
                foreach (PropertyInfo property in propertyInfoArray)
                {
                    if (property.Name == propertyName)
                    {
                        Type tProp = property.PropertyType;
                        if (tProp.IsGenericType
                            && tProp.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                        {
                            if (newValue == null)
                            {
                                property.SetValue(type, null, null);
                                return false;
                            }

                            tProp = new NullableConverter(property.PropertyType).UnderlyingType;
                        }

                        property.SetValue(type, Convert.ChangeType(newValue, tProp, CultureInfo.InvariantCulture), null);
                        isPropertySet = true;
                    }
                }
            }
            catch (Exception ex)
            {
                isPropertySet = false;
            }

            return isPropertySet;
        }

        public static List<GPFMenusDto> GetAllMenu(Claim userClaim)
        {
            var lstAllMenu = new List<GPFMenusDto>();
            if (userClaim != null && !string.IsNullOrWhiteSpace(userClaim.Value))
            {
                string userInfo = userClaim.Value;
                lstAllMenu = JsonConvert.DeserializeObject<List<GPFMenusDto>>(userInfo);
            }

            return lstAllMenu;
        }

        public static List<DropDownViewModel> GetAllChildrens(Claim userClaim)
        {
            var childrens = new List<DropDownViewModel>();
            if (userClaim != null && !string.IsNullOrWhiteSpace(userClaim.Value))
            {
                string userInfo = userClaim.Value;
                childrens = JsonConvert.DeserializeObject<List<DropDownViewModel>>(userInfo);
            }

            return childrens;
        }

        public static string SpellNumber(string numb, bool IsMoney)
        {
            string val = string.Empty;
            string wholeNo = numb;
            string points = string.Empty;
            string andStr = string.Empty;
            string pointStr = string.Empty;
            string endStr = string.Empty;

            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (IsMoney)
                {
                    if (decimalPlace < 0)
                    {
                        // If numb <> "0" Then
                        numb = numb + ".00";
                        // End If
                        decimalPlace = numb.IndexOf(".");
                    }
                }

                if (decimalPlace > 0 & IsMoney)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);


                    if (Convert.ToInt32(points) > 0)
                    {
                        if (points.Length == 1)
                        {
                            points = points + "0";
                        }
                        andStr = "and";
                        endStr = "Cents";
                        pointStr = ConvertWholeNumber(points);
                    }
                    else if (IsMoney)
                    {
                        andStr = "and";
                        endStr = "Cents";
                        pointStr = "No";
                    }
                }
                if (IsMoney)
                {
                    double wholeNumber = (Convert.ToDouble(wholeNo));
                    if (wholeNumber > 9)
                        val = string.Format("{0} {1} {2} {3} {4}", ConvertWholeNumber(wholeNo).Trim(), "Dollars", andStr, pointStr, endStr);
                    else if (wholeNo == "0")
                        val = string.Format("{0} {1} {2} {3} {4}", "Zero ", "Dollar", andStr, pointStr, endStr);
                    else
                        val = string.Format("{0} {1} {2} {3} {4}", ConvertWholeNumber(wholeNo).Trim(), "Dollar", andStr, pointStr, endStr);
                }
                else
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    val = string.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
                }


                if (val.Trim() == "0" | val.Trim() == "")
                    val = "Zero Dollar";
                else if (decimalPlace < 0 & IsMoney)
                    val = val + "Dollars";
            }
            catch
            {
            }


            return val;
        }

        private static string ConvertWholeNumber(string Number)
        {
            string word = "";


            try
            {
                bool beginsZero = false;
                bool isDone = false;
                double dblAmt = (Convert.ToDouble(Number));


                if (dblAmt > 0)
                {
                    beginsZero = Number.StartsWith("0");
                    int numDigits = Number.Length;
                    int pos = 0;
                    string place = "";


                    switch (numDigits)
                    {
                        case 1:
                            {
                                word = ones(Number);
                                isDone = true;
                                break;
                            }


                        case 2:
                            {
                                word = tens(Number);
                                isDone = true;
                                break;
                            }


                        case 3:
                            {
                                pos = (numDigits % 3) + 1;
                                place = " Hundred ";
                                break;
                            }


                        case 4:
                        case 5:
                        case 6:
                            {
                                pos = (numDigits % 4) + 1;
                                place = " Thousand ";
                                break;
                            }


                        case 7:
                        case 8:
                        case 9:
                            {
                                pos = (numDigits % 7) + 1;
                                place = " Million ";
                                break;
                            }


                        case 10:
                        case 11:
                        case 12:
                            {
                                pos = (numDigits % 10) + 1;
                                place = " Billion ";
                                break;
                            }


                        default:
                            {
                                isDone = true;
                                break;
                            }
                    }


                    if (!isDone)
                    {
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch
                            {
                            }
                        }
                        else
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                    }


                    if (word.Trim().Equals(place.Trim()))
                        word = "";
                }
            }
            catch
            {
            }


            return word.Trim();
        }

        private static string ones(string Number)
        {
            int _Number = Convert.ToInt32(Number);
            string name = "";


            switch (_Number)
            {
                case 1:
                    {
                        name = "One";
                        break;
                    }


                case 2:
                    {
                        name = "Two";
                        break;
                    }


                case 3:
                    {
                        name = "Three";
                        break;
                    }


                case 4:
                    {
                        name = "Four";
                        break;
                    }


                case 5:
                    {
                        name = "Five";
                        break;
                    }


                case 6:
                    {
                        name = "Six";
                        break;
                    }


                case 7:
                    {
                        name = "Seven";
                        break;
                    }


                case 8:
                    {
                        name = "Eight";
                        break;
                    }


                case 9:
                    {
                        name = "Nine";
                        break;
                    }
            }


            return name;
        }

        private static string tens(string Number)
        {
            int _Number = Convert.ToInt32(Number);
            string name = null;


            switch (_Number)
            {
                case 10:
                    {
                        name = "Ten";
                        break;
                    }


                case 11:
                    {
                        name = "Eleven";
                        break;
                    }


                case 12:
                    {
                        name = "Twelve";
                        break;
                    }


                case 13:
                    {
                        name = "Thirteen";
                        break;
                    }


                case 14:
                    {
                        name = "Fourteen";
                        break;
                    }


                case 15:
                    {
                        name = "Fifteen";
                        break;
                    }


                case 16:
                    {
                        name = "Sixteen";
                        break;
                    }


                case 17:
                    {
                        name = "Seventeen";
                        break;
                    }


                case 18:
                    {
                        name = "Eighteen";
                        break;
                    }


                case 19:
                    {
                        name = "Nineteen";
                        break;
                    }


                case 20:
                    {
                        name = "Twenty";
                        break;
                    }


                case 30:
                    {
                        name = "Thirty";
                        break;
                    }


                case 40:
                    {
                        name = "Forty";
                        break;
                    }


                case 50:
                    {
                        name = "Fifty";
                        break;
                    }


                case 60:
                    {
                        name = "Sixty";
                        break;
                    }


                case 70:
                    {
                        name = "Seventy";
                        break;
                    }


                case 80:
                    {
                        name = "Eighty";
                        break;
                    }


                case 90:
                    {
                        name = "Ninety";
                        break;
                    }


                default:
                    {
                        if (_Number > 0)
                            name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                        break;
                    }
            }


            return name;
        }
    }
}
