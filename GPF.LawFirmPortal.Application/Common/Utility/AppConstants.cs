﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.Common.Utility
{
    public class AppConstants
    {        
        public static class GPFSettings
        {
            public const string BASEURL = "Appsettings:BaseUrl";

            public const string DOMAINNAME = "Appsettings:LdapSettings:DomainName";

            public const string ADMINEMAIL = "Appsettings:AdminEmail";

            public const string ADMINNAME = "Appsettings:AdminName";

            public const string REQUESTPAYOFFEMAIL = "Appsettings:RequestPayOffEmail";

            public const string MANAGEMENTNAME = "Appsettings:ManagementName";

            public const string HIGLENDEREMAIL = "Appsettings:HIGLenderEmail";

            public const string HIGLENDERNAME = "Appsettings:HIGLenderName";

            public const string MIDCAPLENDERNAME = "Appsettings:MidCapLenderName";

            public const string MIDCAPLENDEREMAIL = "Appsettings:MidCapLenderEmail";

            public const string SESSIONTIMEOUT = "Appsettings:SessionTimeOut";

            public const string SUCCESSMSG = "SuccessMessage";

            public const string ERRORMSG = "ErrorMessage";
            public const string NOLOGO = "";
            public const string LFPORTALUSERNAME = "LFPortal";
        }       
        public static class DocumentTypes
        {
            public const string SUBAGREEMENT = "Subordination Agreements";

            public const string SAMPLEPN = "Sample Promissory Note";

            public const string W9FORM = "W9 Form";

            public const string PROMISSORYNOTE = "Promissory Note";
        }

        public static class StatusTypes
        {
            public const int ACTIVE = 1;

            public const int POTENTIAL = 3;

            public const int INACTIVE = 2;
        }

        public static class EmailSettings
        {
            public const string FROMEMAIL = "Appsettings:EmailSettings:FromEmail";
            public const string FROMNAME = "Appsettings:EmailSettings:FromName";
            public const string PORT = "Appsettings:EmailSettings:Port";
            public const string PASSWORD = "Appsettings:EmailSettings:Password";
            public const string HOST = "Appsettings:EmailSettings:Host";
            public const string ENABLESSL = "Appsettings:EmailSettings:EnableSSL";
            public const string TOEMAIL = "Appsettings:EmailSettings:ToEmail";
        }

        public static class SMSSettings
        {
            public const string ACCOUNTSID = "Appsettings:TwilioSMSSettings:AccountSID";
            public const string AUTHTOKEN = "Appsettings:TwilioSMSSettings:AuthToken";
            public const string FROM = "Appsettings:TwilioSMSSettings:From";
            public const string COUNTRYCODE = "Appsettings:TwilioSMSSettings:CountryCode";
        }

        public static class DocuSignSettings
        {
            public const string USERNAME = "Appsettings:DocuSignSettings:UserName";
            public const string PASSWORD = "Appsettings:DocuSignSettings:Password";
            public const string INTEGRATORKEY = "Appsettings:DocuSignSettings:IntegratorKey";
            public const string ACCOUNTID = "Appsettings:DocuSignSettings:AccountId";
            public const string WEBHOOKURL = "Appsettings:DocuSignSettings:WebhookUrl";
            public const string BASEPATH = "Appsettings:DocuSignSettings:BasePath";
        }

        public static class AmazonS3Settings
        {
            public const string AWSACCESSKEYID = "Appsettings:AmazonS3Settings:AwsAccessKeyId";
            public const string AWSSECRETACCESSKEY = "Appsettings:AmazonS3Settings:AwsSecretAccessKey";
            public const string BUCKETNAME = "Appsettings:AmazonS3Settings:BucketName";
            public const string USECREDENTIALS = "Appsettings:AmazonS3Settings:UseCredentials";
        }

        public static class LDAPSettings
        {
            public const string DOMAIN = "Appsettings:LdapSettings:Domain";
            public const string LDAPIP = "Appsettings:LdapSettings:LDAPIP";
            public const string LDAPPORT = "Appsettings:LdapSettings:LDAPPort";
            public const string LDAPPATH = "Appsettings:LdapSettings:Path";
            public const string VALIDLOGINGROUPS = "Appsettings:LdapSettings:ValidLoginGroups";
            public const string ADMINROLE = "Appsettings:LdapSettings:AdminRole";
        }

        public static class SpConstants
        {
            public const string DASHBOARDINFO = "spDashboardData";
            public const string DASHBOARDCHARTINFO = "spDashboardChartData";
            public const string DASHBOARDCHARCASETYPE = "spDashboardChartCaseType";
            public const string DASHBOARDCHARPERCENTAGE = "spDashboardChartPercentageSettle";
            public const string REQUESTPAYOFFEMAILDATA = "spPayOffData";
        }

        public enum EmailTemplates
        {
            RequestPayOff = 1,
            CaseUpdate = 2
        }
    }
    public class Appsettings
    {
        public LdapSettings LdapSettings { get; set; }
        public bool ShowDesktopNotification { get; set; }
        public string APIUrl { get; set; }
        public string PyrusUrl { get; set; }
        public string AppVersion { get; set; }
    }
    public class LdapSettings
    {
        public string AdminRole { get; set; }
    }
}
