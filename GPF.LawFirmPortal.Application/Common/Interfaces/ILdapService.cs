﻿using System;
using System.Collections.Generic;
using System.Text;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Domain.Common;

namespace GPF.LawFirmPortal.Application.Common.Interfaces
{
    public interface ILdapService
    {
        bool ValidateCredentials(string userName, string password);
        LdapUser ValidateADDirectory(string userName, string password);       
    }
}
