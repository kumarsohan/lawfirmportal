﻿using System.Collections.Generic;

namespace GPF.LawFirmPortal.Application.Common.Interfaces
{
    public interface IUserConnectionManager
    {
        void KeepUserConnection(string userId, string connectionId);
        void RemoveUserConnection(string connectionId);
        List<string> GetUserConnections(string userId);
    }
}