﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GPF.LawFirmPortal.Application.Notifications.Models;

namespace GPF.LawFirmPortal.Application.Common.Interfaces
{
    public interface IEmailSender
    {
        Task SendEmailAsync(MessageDto message, byte[] fileBytes = null, Dictionary<string, string> placeHolders = null, string fileName = null);
    }
}
