﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.Common.Interfaces
{
    public interface ICommonService
    {
        string GetLawFirmName(int LawFirmId);
    }
}
