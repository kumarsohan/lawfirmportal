﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.Common.Interfaces
{
    public interface ICurrentUserService
    {
        public string UserName { get; }
    }
}
