﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.Common.Interfaces
{
    public interface IUnitOfWork : IDisposable, ISprocRepository
    {
        void Add<T>(T obj) where T : class;

        T GetById<T>(int? id) where T : class;

        T AddObj<T>(T obj) where T : class;

        void Update<T>(T obj) where T : class;

        void Remove<T>(T obj) where T : class;

        IQueryable<T> Query<T>() where T : class;

        void Commit();

        Task CommitAsync(CancellationToken cancellationToken);

        void Attach<T>(T obj) where T : class;

        IList<T> ExecuteStoredProcedure<T>(DbCommand command) where T : class;

        dynamic ExecuteMultipleSetProc(DbCommand command, List<string> multipleClasses);

        void AddGPFSystem<T>(T obj) where T : class;

        T GetByIdGPFSystem<T>(int? id) where T : class;

        T AddObjGPFSystem<T>(T obj) where T : class;

        void UpdateGPFSystem<T>(T obj) where T : class;

        void RemoveGPFSystem<T>(T obj) where T : class;

        IQueryable<T> QueryGPFSystem<T>() where T : class;

        void CommitGPFSystem();

        Task CommitAsyncGPFSystem(CancellationToken cancellationToken);

        void AttachGPFSystem<T>(T obj) where T : class;
    }

    public interface ISprocRepository
    {
        DbCommand GetStoredProcedure(string name, params (string, object)[] nameValueParams);

        DbCommand GetStoredProcedure(string name);
    }
}
