﻿using System.Threading.Tasks;
using GPF.LawFirmPortal.Application.Common.Models;

namespace GPF.LawFirmPortal.Application.Common.Interfaces
{
    public interface IIdentityService
    {
        Task<string> GetUserNameAsync(string userId);

        //Task<bool> IsUserExists(int investorId);

        //Task<Result> UpdateUserEmail(int investorId, string emailId);
    }
}
