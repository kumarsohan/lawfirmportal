﻿using AutoMapper;
using GPF.LawFirmPortal.Application.Common.Mappings;
using GPF.LawFirmPortal.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.Admin.ViewModel
{
    public class UserActivityViewModel: IMapFrom<VwUserActivity>
    {
        public string UserName { get; set; }
        public string MachineName { get; set; }
        public string ActionName { get; set; }
        public DateTime? VisitDate { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<VwUserActivity, UserActivityViewModel>()
                .ForMember(d => d.ActionName, opt => opt.MapFrom(s => $"{s.ActionName} "))
                .ForMember(d => d.MachineName, opt => opt.MapFrom(s => $"{s.MachineName}"))
                .ForMember(d => d.UserName, opt => opt.MapFrom(s => $"{s.UserName}"))
                .ForMember(d => d.VisitDate, opt => opt.MapFrom(s => s.VisitDate));
        }
    }
}
