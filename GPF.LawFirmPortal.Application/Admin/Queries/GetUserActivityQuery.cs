﻿using AutoMapper;
using GPF.LawFirmPortal.Application.Admin.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.Admin.Queries
{
   public class GetUserActivityQuery:IRequest<IEnumerable<UserActivityViewModel>>
    {
        public class Handler : IRequestHandler<GetUserActivityQuery, IEnumerable<UserActivityViewModel>>
        {
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMapper _mapper;

            public Handler(IUnitOfWork unitOfWork, IMapper mapper)
            {
                _unitOfWork = unitOfWork;
                _mapper = mapper;
            }
            public async Task<IEnumerable<UserActivityViewModel>> Handle(GetUserActivityQuery request, CancellationToken cancellationToken)
            {
                var userActivities = _unitOfWork.Query<VwUserActivity>();
                return _mapper.Map<IEnumerable<UserActivityViewModel>>(userActivities);
            }           
        }
    }
}
