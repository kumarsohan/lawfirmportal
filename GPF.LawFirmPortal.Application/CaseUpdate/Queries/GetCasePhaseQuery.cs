﻿using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.CaseUpdate.Queries
{
    public class GetCasePhaseQuery: IRequest<List<CasePhaseMapViewModel>>
    {
        public class Handler : IRequestHandler<GetCasePhaseQuery, List<CasePhaseMapViewModel>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<List<CasePhaseMapViewModel>> Handle(GetCasePhaseQuery request, CancellationToken cancellationToken)
            {
                var entities = _unitOfWork.Query<CasePhaseMap>().Where(c => c.RecordStatus == 1).OrderBy(o => o.DisplayOrder).Select(x => new CasePhaseMapViewModel
                {
                    LfcasePhaseId = x.LfcasePhaseId,
                    LfcasePhaseName = x.LfcasePhaseName,
                    IsDropdown = x.IsDropdown,
                    DisplayOrder = x.DisplayOrder
                }).ToList();
                return entities;
            }
        }
    }
}
