﻿using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static GPF.LawFirmPortal.Application.Common.Utility.AppConstants;

namespace GPF.LawFirmPortal.Application.CaseUpdate.Queries
{
    public class GetPayOffDataQuery : IRequest<RequestPayOffData>
    {
        public int PortFolioId { get; set; }
        public class Handler : IRequestHandler<GetPayOffDataQuery, RequestPayOffData>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<RequestPayOffData> Handle(GetPayOffDataQuery request, CancellationToken cancellationToken)
            {

                var spCommand = _unitOfWork.GetStoredProcedure(SpConstants.REQUESTPAYOFFEMAILDATA, ("portfolioId", request.PortFolioId));
                var requestPayOffs = _unitOfWork.ExecuteStoredProcedure<RequestPayOffData>(spCommand);
                return requestPayOffs.FirstOrDefault();
            }
        }
    }
}
