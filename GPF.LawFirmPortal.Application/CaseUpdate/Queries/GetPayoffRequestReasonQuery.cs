﻿using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.CaseUpdate.Queries
{
    public class GetPayoffRequestReasonQuery : IRequest<List<DropDownViewModel>>
    {
        public class Handler : IRequestHandler<GetPayoffRequestReasonQuery, List<DropDownViewModel>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<List<DropDownViewModel>> Handle(GetPayoffRequestReasonQuery request, CancellationToken cancellationToken)
            {
                var entities = _unitOfWork.Query<PayoffRequestReason>().Where(x => x.RecordStatus == true).OrderBy(o => o.Reason)
                    .Select(x => new DropDownViewModel
                    {
                        Text = x.Reason,
                        Value = x.Id
                    }).ToList();
                return entities;
            }
        }
    }
}
