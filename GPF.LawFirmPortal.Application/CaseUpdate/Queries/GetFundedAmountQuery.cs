﻿using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.CaseUpdate.Queries
{
    public class GetFundedAmountQuery : IRequest<List<FundedAmountViewModel>>
    {
        public int CaseId { get; set; }
        public class Handler : IRequestHandler<GetFundedAmountQuery, List<FundedAmountViewModel>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<List<FundedAmountViewModel>> Handle(GetFundedAmountQuery request, CancellationToken cancellationToken)
            {
                var entities = _unitOfWork.Query<VwAmountFundedDetail>().Where(x => x.CaseId == request.CaseId).Select(x => new FundedAmountViewModel
                {
                    Amount = x.Amount,
                    CheckDate = x.CheckDate,
                    ClientTypeName = x.ClientTypeName,
                    DisbursementType = x.DisbursementType
                }).ToList();
                return entities;
            }
        }
    }
}
