﻿using AutoMapper;
using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.CaseUpdate.Queries
{
   public class GetCaseDueforUpdatesQuery : IRequest<IEnumerable<CaseDueUpdatesViewModel>>
    {
        public List<int> LawFirmId { get; set; }
        public class Handler : IRequestHandler<GetCaseDueforUpdatesQuery, IEnumerable<CaseDueUpdatesViewModel>>
        {
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMapper _mapper;

            public Handler(IUnitOfWork unitOfWork, IMapper mapper)
            {
                _unitOfWork = unitOfWork;
                _mapper = mapper;
            }
            public async Task<IEnumerable<CaseDueUpdatesViewModel>> Handle(GetCaseDueforUpdatesQuery request, CancellationToken cancellationToken)
            {
                var lawFirmName = GetLawFirmName(request.LawFirmId);
                var caseDueForUpdate = _unitOfWork.Query<VwOpenDueCaseUpdate>().Where(c => c.NoOfDaysLeft <= -30
                                                                                         && lawFirmName.Contains(c.LawFirmName)
                                                                                         && (c.Reason != null && (c.Reason != "Switched Attorneys" || c.Reason != "Dropped"))
                                                                                        && (c.Status == "Open" || c.Status == "Under Management")
                                                                                        && (c.CaseStatus != "Approved WRITE OFF" || c.CaseStatus != "Pending WRITE OFF Review"
                                                                                        || c.CaseStatus != "Legal (with Counsel)"));

                var openCases = _unitOfWork.Query<VwOpenDueCaseUpdate>().Where(c => lawFirmName.Contains(c.LawFirmName) && (c.Status == "Open"));
                caseDueForUpdate = caseDueForUpdate.Union(openCases).OrderByDescending(s => s.StatusDateEntered);
                return _mapper.Map<IEnumerable<CaseDueUpdatesViewModel>>(caseDueForUpdate);
            }
            private List<string> GetLawFirmName(List<int> lstLawfirmIds)
            {
                List<string> lstLawFirmNames = new List<string>();
                if (lstLawfirmIds.Count == 0 || lstLawfirmIds[0] == 0)
                {
                    lstLawFirmNames.Add("GPFNOLAWFIRMASSIGNFIRSTTIME");
                    return lstLawFirmNames;
                }
                foreach (var item in lstLawfirmIds)
                {
                    var lawFirmName = _unitOfWork.Query<VwLawFirm>().FirstOrDefault(f => f.LawFirmId == item);
                    if (lawFirmName != null)
                        lstLawFirmNames.Add(lawFirmName.Name);
                }
                return lstLawFirmNames;
            }
        }
    }
}
