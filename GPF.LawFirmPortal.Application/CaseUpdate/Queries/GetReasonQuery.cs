﻿using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.CaseUpdate.Queries
{
    public class GetReasonQuery: IRequest<List<CaseStatusDetailMapViewModel>>
    {
        public class Handler : IRequestHandler<GetReasonQuery, List<CaseStatusDetailMapViewModel>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<List<CaseStatusDetailMapViewModel>> Handle(GetReasonQuery request, CancellationToken cancellationToken)
            {
                var entities = _unitOfWork.Query<CaseStatusDetailsMap>().Where(x => x.RecordStatus == 1).OrderBy(o => o.LfcaseDetailsName)
                    .Select(x => new CaseStatusDetailMapViewModel
                    {
                        CasePhaseId = x.CasePhaseId,
                        CasePhaseName = x.CasePhaseName,
                        CaseStatusReasonId = x.CaseStatusReasonId,
                        CaseStatusReasonName = x.CaseStatusReasonName,
                        Category = x.Category,
                        Id=x.Id,
                        CategoryId = x.CategoryId,
                        LfcaseDetailsName = x.LfcaseDetailsName,
                        LfcasePhaseId=x.LfcasePhaseId,
                        Status = x.Status,
                        StatusId = x.StatusId
                    }).ToList();
                return entities;
            }
        }
    }
}
