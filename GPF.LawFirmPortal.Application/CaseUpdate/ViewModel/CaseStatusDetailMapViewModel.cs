﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.CaseUpdate.ViewModel
{
    public class CaseStatusDetailMapViewModel
    {
        public int Id { get; set; }
        public int? CaseStatusReasonId { get; set; }
        public string CaseStatusReasonName { get; set; }
        public int? LfcasePhaseId { get; set; }
        public string LfcaseDetailsName { get; set; }
        public string Category { get; set; }
        public int? CategoryId { get; set; }
        public string Status { get; set; }
        public int? StatusId { get; set; }
        public int? CasePhaseId { get; set; }
        public string CasePhaseName { get; set; }
        public short? RecordStatus { get; set; }
    }
}
