﻿using AutoMapper;
using GPF.LawFirmPortal.Application.Common.Mappings;
using GPF.LawFirmPortal.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.CaseUpdate.ViewModel
{
    public class CaseDueUpdatesViewModel : IMapFrom<VwOpenDueCaseUpdate>
    {
        public int PortfolioId { get; set; }
        public int CaseId { get; set; }
        public int? NoOfDaysLeft { get; set; }
        public string CombinedName { get; set; }
        public string LawFirmdName { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? DOL { get; set; }
        public decimal? ApprovalAmount { get; set; }
        public decimal? AmountOwed { get; set; }
        public int CasePhaseID { get; set; }
        public int CaseStatusReasonID { get; set; }
        public string Note { get; set; }
        public bool IsRequestPayoff { get; set; }
        public DateTime? StatusDateEntered { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public string CaseStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CaseStatusReasonName { get; set; }
        public string Category { get; set; }
        public int CategoryId { get; set; }
        public int StatusId { get; set; }
        public string CasePhaseName { get; set; }
        public int LFCasePhaseId { get; set; }
        public int LFCaseStatusDetailId { get; set; }
        public string LFCaseDetailsName { get; set; }
        public string TrialDateAssigned { get; set; }
        public DepositionsStatus DepositionsStatus { get; set; }
        public string DepositionsStatusName { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<VwOpenDueCaseUpdate, CaseDueUpdatesViewModel>()
                 .ForMember(d => d.PortfolioId, opt => opt.MapFrom(s => $"{s.PortfolioId} "))
                .ForMember(d => d.NoOfDaysLeft, opt => opt.MapFrom(s => $"{s.NoOfDaysLeft}"))
                .ForMember(d => d.DOB, opt => opt.MapFrom(s => $"{s.Dob}"))
                .ForMember(d => d.DOL, opt => opt.MapFrom(s => $"{s.Dol}"))
                .ForMember(d => d.CombinedName, opt => opt.MapFrom(s => $"{s.CombinedName}"))
                .ForMember(d => d.ApprovalAmount, opt => opt.MapFrom(s => s.ApprovalAmount))
                .ForMember(d => d.CasePhaseID, opt => opt.MapFrom(s => s.CasePhaseId))
                .ForMember(d => d.CaseStatusReasonID, opt => opt.MapFrom(s => s.CaseStatusReasonId))
                .ForMember(d => d.CaseId, opt => opt.MapFrom(s => s.CaseId))
                .ForMember(d => d.Note, opt => opt.MapFrom(s => s.Note))
                .ForMember(d => d.LawFirmdName, opt => opt.MapFrom(s => s.LawFirmName))
                .ForMember(d => d.Status, opt => opt.MapFrom(s => s.Status))
                .ForMember(d => d.Reason, opt => opt.MapFrom(s => s.Reason))
                .ForMember(d => d.CaseStatus, opt => opt.MapFrom(s => s.CaseStatus))
                .ForMember(d => d.CreatedBy, opt => opt.MapFrom(s => s.CreatedBy))
                .ForMember(d => d.CreatedDate, opt => opt.MapFrom(s => s.CreatedDate))
                .ForMember(d => d.AmountOwed, opt => opt.MapFrom(s => s.ContractCv));
        }
    }
    public enum DepositionsStatus
    {
        [System.ComponentModel.DataAnnotations.Display(Name = "In Progress")]
        InProgress,
        [System.ComponentModel.DataAnnotations.Display(Name = "Complete")]
        Complete
    }
}
