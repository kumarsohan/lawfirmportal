﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.CaseUpdate.ViewModel
{
    public class RequestPayOffData
    {
        public decimal? Advance { get; set; }
        public decimal? AmountOwed { get; set; }
        public decimal? ApprovedReduction { get; set; }
        public string ClientName { get; set; }
        public string SalesPerson { get; set; }
        public string FinalSalesPerson { get; set; }
        public string LawFirm { get; set; }
        public string InsuranceCompany { get; set; }
        public string IndexNumber { get; set; }
        public string County { get; set; }
        public string MostRecentSP { get; set; }
        public string Category { get; set; }
        public string CurrentStatus { get; set; }
        public string Reason { get; set; }
        public decimal? CV_Clientlevel { get; set; }
        public string StateLF { get; set; }
        public string POPriority { get; set; }
        public string POType { get; set; }
        public string PORequestedBy { get; set; }
        public string PORequestersName { get; set; }
        public string POContractType { get; set; }
        public int PODaysOut { get; set; }
        public decimal? SettledFor { get; set; }
        public decimal? Liens { get; set; }
        public DateTime? EstPaymentDate { get; set; }
        public decimal? SuggestedAmt { get; set; }
        public decimal? ClientNet { get; set; }
        public DateTime? red_approvaldate { get; set; }
        public decimal? red_approvedamt { get; set; }
        public string ContactPerson { get; set; }
        public string Called { get; set; }
        public string Emailed { get; set; }
        public string Faxed { get; set; }
        public string CaseStateCurrentLF { get; set; }
        public string CaseLawFirm { get; set; }
        public bool? eCourts { get; set; }
    }
}
