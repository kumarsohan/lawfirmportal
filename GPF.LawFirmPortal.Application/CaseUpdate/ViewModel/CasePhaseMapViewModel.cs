﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.CaseUpdate.ViewModel
{
   public class CasePhaseMapViewModel
    {
        public int LfcasePhaseId { get; set; }
        public string LfcasePhaseName { get; set; }
        public bool? IsDropdown { get; set; }
        public short? RecordStatus { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
