﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.CaseUpdate.ViewModel
{
    public class FundedAmountViewModel
    {
        public decimal? Amount { get; set; }
        public string ClientTypeName { get; set; }
        public string DisbursementType { get; set; }
        public DateTime? CheckDate { get; set; }
    }
}
