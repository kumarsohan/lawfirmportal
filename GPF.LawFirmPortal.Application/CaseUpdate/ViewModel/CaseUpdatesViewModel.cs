﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.CaseUpdate.ViewModel
{
    public class CaseUpdatesViewModel
    {
        public int Id { get; set; }
        public int? PortfolioId { get; set; }
        public int? CaseId { get; set; }
        public string Comment { get; set; }
        public int? CasePhaseId { get; set; }
        public int? CaseStatusDetailId { get; set; }
        public bool? IsAttorneyComment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? RecordStatus { get; set; }
    }
}
