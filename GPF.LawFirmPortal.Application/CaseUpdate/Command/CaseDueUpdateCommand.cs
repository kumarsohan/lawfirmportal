﻿using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.CaseUpdate.Command
{
   public class CaseDueUpdateCommand : IRequest<bool>
    {
        public CaseUpdatesViewModel caseUpdatesAuditViewModel { get; set; }

        public class Handler : IRequestHandler<CaseDueUpdateCommand, bool>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<bool> Handle(CaseDueUpdateCommand request, CancellationToken cancellationToken)
            {
                bool isUpdated = false;
                var source = _unitOfWork.Query<CaseUpdates>().OrderByDescending(o=>o.CreatedDate).FirstOrDefault(a => a.PortfolioId == request.caseUpdatesAuditViewModel.PortfolioId);

                if (source != null)
                {
                    source.RecordStatus = false;
                }
                await _unitOfWork.CommitAsync(cancellationToken).ConfigureAwait(true);
                var CaseUpdate = new CaseUpdates();
                CaseUpdate.CaseId = request.caseUpdatesAuditViewModel.CaseId;
                CaseUpdate.PortfolioId = request.caseUpdatesAuditViewModel.PortfolioId;
                CaseUpdate.Comment = request.caseUpdatesAuditViewModel.Comment;
                CaseUpdate.CasePhaseId = request.caseUpdatesAuditViewModel.CasePhaseId;
                CaseUpdate.CaseStatusDetailId = request.caseUpdatesAuditViewModel.CaseStatusDetailId;
                CaseUpdate.CreatedBy = request.caseUpdatesAuditViewModel.CreatedBy;
                CaseUpdate.CreatedDate = DateTime.Now;
                CaseUpdate.IsAttorneyComment = request.caseUpdatesAuditViewModel.IsAttorneyComment;
                CaseUpdate.RecordStatus = true;
                _unitOfWork.Add(CaseUpdate);
                await _unitOfWork.CommitAsync(cancellationToken).ConfigureAwait(true);
                isUpdated = true;
                return isUpdated;
            }
        }
    }
}
