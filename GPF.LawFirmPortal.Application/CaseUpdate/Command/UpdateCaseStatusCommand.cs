﻿using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static GPF.LawFirmPortal.Application.Common.Utility.AppConstants;

namespace GPF.LawFirmPortal.Application.CaseUpdate.Command
{
    public class UpdateCaseStatusCommand : IRequest<bool>
    {
       public CaseDueUpdatesViewModel caseDueUpdatesViewModel { get; set; }
        public bool isRequestPayOff { get; set; }
        public class Handler : IRequestHandler<UpdateCaseStatusCommand, bool>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<bool> Handle(UpdateCaseStatusCommand request, CancellationToken cancellationToken)
            {
                bool isUpdated = false;
                var source = _unitOfWork.QueryGPFSystem<StatusEntries>().OrderByDescending(o => o.Dateentered).
                    First(a => a.PortfolioId == request.caseDueUpdatesViewModel.PortfolioId);
                source.Id = 0;
                if (request.caseDueUpdatesViewModel.CasePhaseID != 0)
                    source.CasePhaseId = request.caseDueUpdatesViewModel.CasePhaseID;
                source.Enteredby = GPFSettings.LFPORTALUSERNAME;
                source.CaseStatusReasonId = request.caseDueUpdatesViewModel.CaseStatusReasonID;
                source.StatusCategory = request.caseDueUpdatesViewModel.Category;
                source.CaseStatusId = request.caseDueUpdatesViewModel.CategoryId;
                source.Reason = request.caseDueUpdatesViewModel.CaseStatusReasonName;
                source.Status = request.caseDueUpdatesViewModel.Status;
                source.CaseContactStatusId = request.caseDueUpdatesViewModel.StatusId;
                source.Note = request.caseDueUpdatesViewModel.Note;
                #region NextFolloupDate Calculation Logic
                if (request.isRequestPayOff == false)
                {

                    var daysLeft = GetEstDaysLeft(request.caseDueUpdatesViewModel.CategoryId, request.caseDueUpdatesViewModel.StatusId, request.caseDueUpdatesViewModel.PortfolioId);
                    var calcNextFollowupDate = DateTime.Now.AddDays(Convert.ToDouble(daysLeft));
                    var statusUpdated = false;

                    if (request.caseDueUpdatesViewModel.Status.ToLower() == "good")
                    {
                        source.LastUpdateReceived = DateTime.Now;
                        statusUpdated = true;
                    }

                    if (statusUpdated)
                    {
                        source.FollowupDate = calcNextFollowupDate;
                    }
                    else
                    {
                        var nextFollowupDateObj = _unitOfWork.Query<VwOpenDueCaseUpdate>().FirstOrDefault(c => c.PortfolioId == request.caseDueUpdatesViewModel.PortfolioId);
                        if (nextFollowupDateObj != null)
                        {
                            source.FollowupDate = DateTime.Now <= nextFollowupDateObj.NextFollowUpDate ? nextFollowupDateObj.NextFollowUpDate : calcNextFollowupDate;
                        }
                    }
                }
                #endregion
                source.Dateentered = DateTime.Now;
                source.EntryDate = DateTime.Now;
                source.LastUpdateReceived = DateTime.Now;
                _unitOfWork.AddGPFSystem<StatusEntries>(source);
                await _unitOfWork.CommitAsyncGPFSystem(cancellationToken).ConfigureAwait(true);
                isUpdated = true;
                return isUpdated;
            }
            private int? GetEstDaysLeft(int? CategoryID, int? StatusId, int id)
            {
                var contactCV = _unitOfWork.Query<VwOpenDueCaseUpdate>().FirstOrDefault(p => p.PortfolioId == id).ContractCv;
                var scheduleDays = _unitOfWork.QueryGPFSystem<FollowUpRules>().Where(fu => fu.StatusId == CategoryID
                                         && fu.SubStatusId == StatusId
                                         && (contactCV >= fu.CappedAmountMin
                                         && contactCV <= fu.CappedAmountMax))
                           .Select(c => new
                           {
                               c.MaxTime
                           });
                var maxTime = scheduleDays.FirstOrDefault() != null ? scheduleDays.FirstOrDefault()?.MaxTime : 30;
                return maxTime;
            }
        }
    }
}
