﻿using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.CaseUpdate.Command
{
    public class UpdateRequestPayOffCommand : IRequest<bool>
    {
        //public CaseDueUpdatesViewModel caseDueUpdatesViewModel { get; set; }
        public int PortfolioId { get; set; }
        public string UserName { get; set; }
        public string Message { get; set; }
        public int ReasonId { get; set; }
        public class Handler : IRequestHandler<UpdateRequestPayOffCommand, bool>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<bool> Handle(UpdateRequestPayOffCommand request, CancellationToken cancellationToken)
            {
                var requestPayOffLog = new RequestPayOffLog();
                requestPayOffLog.PortfolioId = request.PortfolioId;
                requestPayOffLog.CaseId = null;
                requestPayOffLog.IsRequestPayoff = true;
                requestPayOffLog.PayoffRequestReasonId = request.ReasonId;
                requestPayOffLog.CreatedDate = DateTime.Now;
                requestPayOffLog.CreatedBy = request.UserName;
                requestPayOffLog.Message = request.Message;
                _unitOfWork.Add(requestPayOffLog);
                await _unitOfWork.CommitAsync(cancellationToken);
                return true;
            }
        }
    }
}
