﻿using AutoMapper;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Notifications.Models;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.Notifications.Queries
{
   public class GetNotificationsQuery : IRequest<IEnumerable<NotificationViewModel>>
    {
        public string LawFirmID { get; set; }
        public bool IsUserAdmin { get; set; }
        public class Handler : IRequestHandler<GetNotificationsQuery, IEnumerable<NotificationViewModel>>
        {
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMapper _mapper;
            public Handler(IUnitOfWork unitOfWork, IMapper mapper)
            {
                _unitOfWork = unitOfWork;
                _mapper = mapper;
            }

            public async Task<IEnumerable<NotificationViewModel>> Handle(GetNotificationsQuery request, CancellationToken cancellationToken)
            {
                var notifications = _unitOfWork.Query<Notification>();
                if (request.IsUserAdmin == true)
                {
                    notifications = notifications.Where(x => x.NotificationState == false);
                }
                else
                {
                    notifications = notifications.Where(x => x.LawFirmId == Convert.ToInt32(request.LawFirmID) && x.NotificationState == false);
                }
                return _mapper.Map<IEnumerable<NotificationViewModel>>(notifications);
            }
        }
    }
}
