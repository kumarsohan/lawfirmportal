﻿using AutoMapper;
using GPF.LawFirmPortal.Application.Common.Mappings;
using GPF.LawFirmPortal.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.Notifications.Models
{
    public class NotificationViewModel : IMapFrom<Notification>
    {
        public int NotificationId { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Message { get; set; }
        public bool? NotificationState { get; set; }
        public DateTime? ReadDate { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Notification, NotificationViewModel>()
                .ForMember(d => d.NotificationId, opt => opt.MapFrom(s => $"{s.NotificationId} "))
                .ForMember(d => d.NotificationState, opt => opt.MapFrom(s => $"{s.NotificationState}"))
                .ForMember(d => d.UserName, opt => opt.MapFrom(s => $"{s.UserName}"))
                .ForMember(d => d.Message, opt => opt.MapFrom(s => s.Message))
                .ForMember(d => d.DisplayName, opt => opt.MapFrom(s => s.DisplayName))
                .ForMember(d => d.ReadDate, opt => opt.MapFrom(s => s.ReadDate));
        }
    }
}
