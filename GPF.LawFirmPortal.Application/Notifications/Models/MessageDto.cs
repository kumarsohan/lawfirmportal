﻿namespace GPF.LawFirmPortal.Application.Notifications.Models
{
    public class MessageDto
    {
        public int TemplateId { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string CC { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}
