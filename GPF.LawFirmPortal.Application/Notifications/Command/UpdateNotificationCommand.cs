﻿using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.Notifications.Command
{
    public class UpdateNotificationCommand: IRequest<bool>
    {
        public string NotificationId { get; set; }
        public class Handler : IRequestHandler<UpdateNotificationCommand, bool>
        {
            private readonly IUnitOfWork _unitOfWork;
            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<bool> Handle(UpdateNotificationCommand request, CancellationToken cancellationToken)
            {
                string[] ids =request.NotificationId.Split(',');
                var notifications = _unitOfWork.Query<Notification>().Where(r => ids.Contains(r.NotificationId.ToString())).ToList();
                foreach (var item in notifications)
                {
                    item.NotificationState = true;
                    item.ReadDate = DateTime.Now;
                }
               //
                await _unitOfWork.CommitAsync(cancellationToken);
                return true;
            }
        }
    }
}
