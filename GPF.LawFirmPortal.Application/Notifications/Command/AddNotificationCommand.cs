﻿using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.Notifications.Command
{
   public class AddNotificationCommand: IRequest<bool>
    {
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string LawFirmID { get; set; }
        public string Message { get; set; }
        public string UserGroupName { get; set; }
        public bool? NotificationState { get; set; }
        public DateTime? ReadDate { get; set; }
        public class Handler : IRequestHandler<AddNotificationCommand, bool>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<bool> Handle(AddNotificationCommand request, CancellationToken cancellationToken)
            {
                var notification = new Notification();
                notification.Message = request.Message;
                notification.NotificationState = request.NotificationState;
                notification.UserName = request.UserName;
                notification.DisplayName = request.DisplayName;
                notification.LawFirmId = request.LawFirmID == "" ? 0 : Convert.ToInt32(request.LawFirmID);
                notification.UserGroupName = request.UserGroupName;
                _unitOfWork.Add(notification);
                await _unitOfWork.CommitAsync(cancellationToken);
                return true;
            }
        }
    }
}
