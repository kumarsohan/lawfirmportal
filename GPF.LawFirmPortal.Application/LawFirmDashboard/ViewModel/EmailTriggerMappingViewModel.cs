﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.LawFirmDashboard.ViewModel
{
    public class EmailTriggerMappingViewModel
    {
        public int CaseStatusDetailId { get; set; }
        public bool? IsRiskMngmntEmailTrigger { get; set; }
        public string EmailMember { get; set; }
        public string DepartmentName { get; set; }
    }
}
