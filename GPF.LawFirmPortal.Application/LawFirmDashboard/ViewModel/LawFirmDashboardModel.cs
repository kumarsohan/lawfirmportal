﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.LawFirmDashboard.ViewModel
{
    public class LawFirmDashboardModel
    {
        public int? CaseDueUpdate { get; set; }
        public int? OpenCases { get; set; }
        public int? SettledCaseCountTotal { get; set; }
        public decimal? TotalPrincipalSettle { get; set; }
        public decimal? TotalSettlementAmount { get; set; }
        public IEnumerable<CasePhaseClientCount> phaseClientCounts { get; set; }
        public IEnumerable<TotalFundingsData> totalFundingChart { get; set; }
        public IEnumerable<CaseTypeBreakdown> caseTypeBreakdownChart { get; set; }
        public IEnumerable<PercentageSettle> percentageSettlesChart { get; set; }
        public string FirmValues { get; set; }
    }
    public class CasePhaseClientCount
    {
        public string CaseStatus { get; set; }
        public int? CasePhaseCount { get; set; }
    }
    public class TotalFundingsData
    {
        public string FundingYear { get; set; }
        public decimal TotalFundingAmount { get; set; }
        public int? TotalFundingCount { get; set; }
        
    }
    public class CaseTypeBreakdown
    {        
        public string CaseType { get; set; }
        public decimal TotalFundingAmount { get; set; }
    }
    public class PercentageSettle
    {
        public int? Percentage { get; set; }
        public string FundingYear { get; set; }
        public string AmountFunded { get; set; }
        public int ClientCount { get; set; }
        public string SettledCases { get; set; }
    }
    public class LawFirmDashboardVMModel
    {
        public int? CaseDueUpdate { get; set; }
        public int? SettledCaseCountTotal { get; set; }
        public decimal? TotalPrincipalSettle { get; set; }
        public decimal? TotalSettlementAmount { get; set; }
        public int? OpenCases { get; set; }
    }
}
