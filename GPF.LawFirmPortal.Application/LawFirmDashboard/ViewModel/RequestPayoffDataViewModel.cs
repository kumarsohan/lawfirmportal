﻿using AutoMapper;
using GPF.LawFirmPortal.Application.Common.Mappings;
using GPF.LawFirmPortal.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPF.LawFirmPortal.Application.LawFirmDashboard.ViewModel
{
    public class RequestPayoffDataViewModel : IMapFrom<RequestPayOffLog>
    {
        public int LogId { get; set; }
        public int? CaseId { get; set; }
        public int PortfolioId { get; set; }
        public int? PayoffRequestReasonID { get; set; }
        public string PayoffRequestReasonName { get; set; }
        public bool? IsRequestPayoff { get; set; }
        public string CombinedName { get; set; }
        public string LawFirmdName { get; set; }
        public string Note { get; set; }
        public string Status { get; set; }
        public bool? RecordStatus { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<RequestPayOffLog, RequestPayoffDataViewModel>()
                 .ForMember(d => d.PortfolioId, opt => opt.MapFrom(s => $"{s.PortfolioId} "))
                .ForMember(d => d.CreatedDate, opt => opt.MapFrom(s => $"{s.CreatedDate}"))
                .ForMember(d => d.CreatedBy, opt => opt.MapFrom(s => $"{s.CreatedBy}"))
                .ForMember(d => d.IsRequestPayoff, opt => opt.MapFrom(s => $"{s.IsRequestPayoff}"));
        }
    }
}
