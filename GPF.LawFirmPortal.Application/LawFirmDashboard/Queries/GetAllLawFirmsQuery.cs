﻿using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.LawFirmDashboard.Queries
{
   public class GetAllLawFirmsQuery: IRequest<List<DropDownViewModel>>
    {
        public class Handler : IRequestHandler<GetAllLawFirmsQuery, List<DropDownViewModel>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<List<DropDownViewModel>> Handle(GetAllLawFirmsQuery request, CancellationToken cancellationToken)
            {
                var entities = _unitOfWork.Query<VwLawFirm>().OrderBy(o=>o.Name).Select(x => new DropDownViewModel
                {
                    Text = x.Name,
                    Value = x.LawFirmId,
                }).ToList();
                return entities;
            }
        }
    }
}
