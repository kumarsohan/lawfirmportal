﻿using AutoMapper;
using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Application.LawFirmDashboard.ViewModel;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using Org.BouncyCastle.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.LawFirmDashboard.Queries
{
   public class GetEmailTriggerMappingQuery : IRequest<EmailTriggerMappingViewModel>
    {
        public int CaseStatusDetailId { get; set; }
        public class Handler : IRequestHandler<GetEmailTriggerMappingQuery, EmailTriggerMappingViewModel>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork, IMapper mapper)
            {
                _unitOfWork = unitOfWork; _mapper = mapper;
            }

            public async Task<EmailTriggerMappingViewModel> Handle(GetEmailTriggerMappingQuery request, CancellationToken cancellationToken)
            {
                var emailTriggerMapping = _unitOfWork.Query<EmailTriggerMapping>().Where(x => x.CaseStatusDetailId == request.CaseStatusDetailId
                                                                                && x.IsRiskMngmntEmailTrigger == true
                                                                                && x.RecordStatus == true).FirstOrDefault();
                if (emailTriggerMapping!=null)
                {
                    return new EmailTriggerMappingViewModel() {
                        CaseStatusDetailId = emailTriggerMapping.CaseStatusDetailId,
                        DepartmentName=emailTriggerMapping.DepartmentName,
                        EmailMember=emailTriggerMapping.EmailMember,
                        IsRiskMngmntEmailTrigger=emailTriggerMapping.IsRiskMngmntEmailTrigger                        
                    };
                }
                return null;
            }
        }
    }
}
