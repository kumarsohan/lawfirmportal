﻿using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.LawFirmDashboard.ViewModel;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GPF.LawFirmPortal.Domain.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using static GPF.LawFirmPortal.Application.Common.Utility.AppConstants;

namespace GPF.LawFirmPortal.Application.LawFirmDashboard.Queries
{
    public class GetLawFirmDataQuery : IRequest<LawFirmDashboardModel>
    {
        public List<int> LawFirmId { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
        public class GetLawFirmDataHandler : IRequestHandler<GetLawFirmDataQuery, LawFirmDashboardModel>
        {
            private readonly IUnitOfWork _unitOfWork;
            public GetLawFirmDataHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }
            public async Task<LawFirmDashboardModel> Handle(GetLawFirmDataQuery request, CancellationToken cancellationToken)
            {
                var lawFirmModel = new LawFirmDashboardModel();
                var lawFirmName = GetLawFirmName(request.LawFirmId);

                //var spCommand = _unitOfWork.GetStoredProcedure(SpConstants.DASHBOARDINFO, ("LawFirmName", string.Join(";", lawFirmName)), ("StartDate", request.StartDate), ("EndDate", request.EndDate));
                var spCommand = _unitOfWork.GetStoredProcedure(SpConstants.DASHBOARDINFO, ("LawFirmName", string.Join(";", lawFirmName)));
                var lawFirmDashboardCount = _unitOfWork.ExecuteStoredProcedure<LawFirmDashboardVMModel>(spCommand);

                //var spCommandChart = _unitOfWork.GetStoredProcedure(SpConstants.DASHBOARDCHARTINFO, ("LawFirmName", string.Join(";", lawFirmName)), ("StartDate", request.StartDate), ("EndDate", request.EndDate));
                //var spCommandChart = _unitOfWork.GetStoredProcedure(SpConstants.DASHBOARDCHARTINFO, ("LawFirmName", string.Join(";", lawFirmName)));
                //var lawFirmDashboardChart = _unitOfWork.ExecuteStoredProcedure<TotalFundingsData>(spCommandChart);


                //var spCommandCaseType = _unitOfWork.GetStoredProcedure(SpConstants.DASHBOARDCHARCASETYPE, ("LawFirmName", string.Join(";", lawFirmName)));
                //var lawFirmDashboardCaseType = _unitOfWork.ExecuteStoredProcedure<CaseTypeBreakdown>(spCommandCaseType);

                //var spCommandPercentage = _unitOfWork.GetStoredProcedure(SpConstants.DASHBOARDCHARPERCENTAGE, ("LawFirmName", string.Join(";", lawFirmName)));
                //var lawFirmDashboardPercentage = _unitOfWork.ExecuteStoredProcedure<PercentageSettle>(spCommandPercentage);                

                if (lawFirmDashboardCount != null)
                {
                    lawFirmModel.SettledCaseCountTotal = lawFirmDashboardCount.FirstOrDefault().SettledCaseCountTotal;
                    lawFirmModel.TotalPrincipalSettle = lawFirmDashboardCount.FirstOrDefault().TotalPrincipalSettle;
                    lawFirmModel.TotalSettlementAmount = lawFirmDashboardCount.FirstOrDefault().TotalSettlementAmount;
                    lawFirmModel.CaseDueUpdate = lawFirmDashboardCount.FirstOrDefault().CaseDueUpdate;
                    lawFirmModel.OpenCases = lawFirmDashboardCount.FirstOrDefault().OpenCases;
                }

                
                //if (lawFirmDashboardChart != null)
                //{
                //    lawFirmModel.totalFundingChart = lawFirmDashboardChart.Select(f => new TotalFundingsData
                //    {
                //        FundingYear = f.FundingYear,
                //        TotalFundingAmount = Math.Round(Convert.ToDecimal(f.TotalFundingAmount), 2),//string.Format("{0:0.00}M", (f.Select(s=>s.Advance).Sum()/ 1000000))
                //        TotalFundingCount = f.TotalFundingCount
                //    }).OrderBy(o => o.FundingYear).ToList();
                //}
                //if (lawFirmDashboardCaseType != null)
                //{
                //    lawFirmModel.caseTypeBreakdownChart = lawFirmDashboardCaseType.Select(f => new CaseTypeBreakdown
                //    {
                //        TotalFundingAmount = f.TotalFundingAmount,
                //        CaseType = f.CaseType
                //    }).ToList();
                //}
                //if (lawFirmDashboardPercentage != null)
                //{
                //    lawFirmModel.percentageSettlesChart = lawFirmDashboardPercentage.Select(f => new PercentageSettle
                //    {
                //        FundingYear = f.FundingYear,
                //        Percentage = f.Percentage,
                //        AmountFunded = f.AmountFunded,
                //        ClientCount = f.ClientCount,
                //        SettledCases = f.SettledCases
                //    }).OrderBy(o => o.FundingYear).ToList();
                //}
                return lawFirmModel;
            }
            private List<string> GetLawFirmName(List<int> lstLawfirmIds)
            {
                List<string> lstLawFirmNames = new List<string>();
                if (lstLawfirmIds.Count == 0 || lstLawfirmIds[0] == 0)
                {
                    lstLawFirmNames.Add("GPFNOLAWFIRMASSIGNFIRSTTIME");
                    return lstLawFirmNames;
                }
                foreach (var item in lstLawfirmIds)
                {
                    var lawFirmName = _unitOfWork.Query<VwLawFirm>().FirstOrDefault(f => f.LawFirmId == item);
                    if (lawFirmName != null)
                        lstLawFirmNames.Add(lawFirmName.Name);
                }
                return lstLawFirmNames;
            }
        }
    }
}
