﻿using AutoMapper;
using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Application.LawFirmDashboard.ViewModel;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using Org.BouncyCastle.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.LawFirmDashboard.Queries
{
   public class GetRequestPayoffQuery : IRequest<RequestPayoffDataViewModel>
    {
        //public int searchType { get; set; }
        public int PortfolioId { get; set; }
        public class Handler : IRequestHandler<GetRequestPayoffQuery, RequestPayoffDataViewModel>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork, IMapper mapper)
            {
                _unitOfWork = unitOfWork; _mapper = mapper;
            }

            public async Task<RequestPayoffDataViewModel> Handle(GetRequestPayoffQuery request, CancellationToken cancellationToken)
            {
                var requestPayOffLogs = _unitOfWork.Query<RequestPayOffLog>().Where(x => x.PortfolioId == request.PortfolioId).OrderByDescending(o => o.CreatedDate).FirstOrDefault();
                return _mapper.Map<RequestPayoffDataViewModel>(requestPayOffLogs);
            }
        }
    }
}
