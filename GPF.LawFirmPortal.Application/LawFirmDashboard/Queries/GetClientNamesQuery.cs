﻿using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using GPF.LawFirmPortal.Domain.Entities;
using MediatR;
using Org.BouncyCastle.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Application.LawFirmDashboard.Queries
{
   public class GetClientNamesQuery : IRequest<List<CaseDueUpdatesViewModel>>
    {
        public SearchType searchType { get; set; }
        public int PortfolioId { get; set; }
        public List<int> LawFirmId { get; set; }
        public class Handler : IRequestHandler<GetClientNamesQuery, List<CaseDueUpdatesViewModel>>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<List<CaseDueUpdatesViewModel>> Handle(GetClientNamesQuery request, CancellationToken cancellationToken)
            {
                if (request.searchType == 0)
                {
                    List<string> lstLawFirmNames = new List<string>();
                    if (request.LawFirmId.Count == 0 || request.LawFirmId[0] == 0)
                    {
                        lstLawFirmNames.Add("GPFNOLAWFIRMASSIGNFIRSTTIME");
                    }
                    else
                    {
                        foreach (var item in request.LawFirmId)
                        {
                            var lawFirmName = _unitOfWork.Query<VwLawFirm>().FirstOrDefault(f => f.LawFirmId == item);
                            if (lawFirmName != null)
                                lstLawFirmNames.Add(lawFirmName.Name);
                        }
                    }
                    var entities = _unitOfWork.Query<VwOpenDueCaseUpdate>().Where(c => c.NoOfDaysLeft <= -30
                                                                                    && lstLawFirmNames.Contains(c.LawFirmName)
                                                                                    && (c.Reason != null && (c.Reason != "Switched Attorneys" || c.Reason != "Dropped"))
                                                                                    && (c.Status == "Open" || c.Status == "Under Management")
                                                                                    && (c.CaseStatus != "Approved WRITE OFF" || c.CaseStatus != "Pending WRITE OFF Review"
                                                                                    || c.CaseStatus != "Legal (with Counsel)")).Select(x => new CaseDueUpdatesViewModel
                                                                                    {
                                                                                        CombinedName = x.CombinedName,
                                                                                        PortfolioId = x.PortfolioId,
                                                                                    }).OrderBy(o => o.CombinedName).ToList();
                    return entities;
                }
                else
                {
                    var entities = _unitOfWork.Query<VwOpenDueCaseUpdate>().Where(f => f.PortfolioId == request.PortfolioId).Select(x => new CaseDueUpdatesViewModel
                    {
                        CombinedName = x.CombinedName,
                        PortfolioId = x.PortfolioId,
                        AmountOwed = x.ContractCv,
                        ApprovalAmount = x.ApprovalAmount,
                        DOB = x.Dob,
                        DOL = x.Dol,
                        NoOfDaysLeft = x.NoOfDaysLeft,
                        CaseId = x.CaseId.Value
                    }).OrderBy(o => o.CombinedName).ToList();
                    return entities;
                }
            }
        }
        public enum SearchType
        {
            All = 0,
            Specific = 1
        }
    }
}
