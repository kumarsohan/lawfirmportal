﻿using System;
using GPF.LawFirmPortal.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace GPF.LawFirmPortal.Infrastructure
{
    public partial class GPFLawFirmPortalContext : DbContext
    {
        public GPFLawFirmPortalContext()
        {
        }

        public GPFLawFirmPortalContext(DbContextOptions<GPFLawFirmPortalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CasePhaseMap> CasePhaseMap { get; set; }
        public virtual DbSet<CaseStatusDetailsMap> CaseStatusDetailsMap { get; set; }
        public virtual DbSet<CaseUpdates> CaseUpdates { get; set; }
        public virtual DbSet<ElmahError> ElmahError { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplate { get; set; }
        public virtual DbSet<EmailTriggerMapping> EmailTriggerMapping { get; set; }
        public virtual DbSet<EventAudit> EventAudit { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<PayoffRequestReason> PayoffRequestReason { get; set; }
        public virtual DbSet<PushSubscription> PushSubscription { get; set; }
        public virtual DbSet<RequestPayOffLog> RequestPayOffLog { get; set; }
        public virtual DbSet<VwAmountFundedDetail> VwAmountFundedDetail { get; set; }
        public virtual DbSet<VwLawFirm> VwLawFirm { get; set; }
        public virtual DbSet<VwOpenDueCaseUpdate> VwOpenDueCaseUpdate { get; set; }
        public virtual DbSet<VwStatusEntries> VwStatusEntries { get; set; }
        public virtual DbSet<VwUserActivity> VwUserActivity { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=WIN-DEV-SQL01;Database=GPFLawFirmPortal;User ID=lfpsvc;Password=WedoWebSh1tH3r3$");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CasePhaseMap>(entity =>
            {
                entity.HasKey(e => e.LfcasePhaseId)
                    .HasName("PK_LFCasePhaseMapping");

                entity.Property(e => e.LfcasePhaseId).HasColumnName("LFCasePhaseID");

                entity.Property(e => e.LfcasePhaseName)
                    .HasColumnName("LFCasePhaseName")
                    .HasMaxLength(1000);
            });

            modelBuilder.Entity<CaseStatusDetailsMap>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CasePhaseName).HasMaxLength(1000);

                entity.Property(e => e.CaseStatusReasonId).HasColumnName("CaseStatusReasonID");

                entity.Property(e => e.CaseStatusReasonName).HasMaxLength(1000);

                entity.Property(e => e.Category).HasMaxLength(1000);

                entity.Property(e => e.LfcaseDetailsName)
                    .HasColumnName("LFCaseDetailsName")
                    .HasMaxLength(1000);

                entity.Property(e => e.LfcasePhaseId).HasColumnName("LFCasePhaseId");

                entity.Property(e => e.Status).HasMaxLength(100);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");
            });

            modelBuilder.Entity<CaseUpdates>(entity =>
            {
                entity.Property(e => e.CreatedBy).HasMaxLength(200);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ElmahError>(entity =>
            {
                entity.HasKey(e => e.ErrorId)
                    .IsClustered(false);

                entity.ToTable("ELMAH_Error");

                entity.HasIndex(e => new { e.Application, e.TimeUtc, e.Sequence })
                    .HasName("IX_ELMAH_Error_App_Time_Seq");

                entity.Property(e => e.ErrorId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AllXml)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.Property(e => e.Application)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.Host)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Sequence).ValueGeneratedOnAdd();

                entity.Property(e => e.Source)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.TimeUtc).HasColumnType("datetime");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EmailTemplate>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateModified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ImageName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Pdfname)
                    .HasColumnName("PDFName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Subject)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmailTriggerMapping>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CaseStatusDetailId)
                    .HasColumnName("CaseStatusDetailID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DepartmentName).HasMaxLength(200);

                entity.Property(e => e.EmailMember).HasMaxLength(500);

                entity.HasOne(d => d.CaseStatusDetail)
                    .WithMany(p => p.EmailTriggerMapping)
                    .HasForeignKey(d => d.CaseStatusDetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmailTriggerMapping_EmailTriggerMapping");
            });

            modelBuilder.Entity<EventAudit>(entity =>
            {
                entity.HasKey(e => e.EventId)
                    .HasName("PK_Event");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventType)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.InsertedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.JsonData).IsRequired();

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.DisplayName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LawFirmId).HasColumnName("LawFirmID");

                entity.Property(e => e.Message).HasMaxLength(4000);

                entity.Property(e => e.ReadDate).HasColumnType("datetime");

                entity.Property(e => e.UserGroupName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PayoffRequestReason>(entity =>
            {
                entity.Property(e => e.Reason).HasMaxLength(100);
            });

            modelBuilder.Entity<PushSubscription>(entity =>
            {
                entity.Property(e => e.Auth).HasMaxLength(500);

                entity.Property(e => e.Endpoint).HasMaxLength(500);

                entity.Property(e => e.P256dh)
                    .HasColumnName("P256Dh")
                    .HasMaxLength(500);

                entity.Property(e => e.UserId).HasMaxLength(50);
            });

            modelBuilder.Entity<RequestPayOffLog>(entity =>
            {
                entity.HasKey(e => e.LogId);

                entity.Property(e => e.CreatedBy).HasMaxLength(200);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.PayoffRequestReasonId).HasColumnName("PayoffRequestReasonID");
            });

            modelBuilder.Entity<VwAmountFundedDetail>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwAmountFundedDetail");

                entity.Property(e => e.Amount).HasColumnType("decimal(14, 2)");

                entity.Property(e => e.CaseId).HasColumnName("CaseID");

                entity.Property(e => e.CheckDate).HasColumnType("datetime");

                entity.Property(e => e.ClientType)
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.ClientTypeName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DisbursementType)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwLawFirm>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwLawFirm");

                entity.Property(e => e.LawFirmId)
                    .HasColumnName("LawFirmID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwOpenDueCaseUpdate>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwOpenDueCaseUpdate");

                entity.Property(e => e.AmountOwed).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.ApprovalAmount).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.CaseId).HasColumnName("CaseID");

                entity.Property(e => e.CasePhase)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CasePhaseId).HasColumnName("CasePhaseID");

                entity.Property(e => e.CasePhaseIdold).HasColumnName("CasePhaseIDOLD");

                entity.Property(e => e.CaseStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CaseStatusId).HasColumnName("CaseStatusID");

                entity.Property(e => e.CaseStatusReasonId).HasColumnName("CaseStatusReasonID");

                entity.Property(e => e.CaseStatusReasonIdold).HasColumnName("CaseStatusReasonIDOLD");

                entity.Property(e => e.CombinedName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCv)
                    .HasColumnName("ContractCV")
                    .HasColumnType("decimal(38, 2)");

                entity.Property(e => e.ContractPrincipal).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.CreatedBy).HasMaxLength(200);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateFunded).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.Dol)
                    .HasColumnName("DOL")
                    .HasColumnType("date");

                entity.Property(e => e.LawFirmName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LfcaseDetailsName)
                    .HasColumnName("LFCaseDetailsName")
                    .HasMaxLength(1000);

                entity.Property(e => e.NextFollowUpDate).HasColumnType("datetime");

                entity.Property(e => e.Reason)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StatusDateEntered).HasColumnType("datetime");
            });

            modelBuilder.Entity<VwStatusEntries>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwStatusEntries");

                entity.Property(e => e.AttorneyFee).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Called)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CaseContactStatusId).HasColumnName("CaseContactStatusID");

                entity.Property(e => e.CaseId).HasColumnName("CaseID");

                entity.Property(e => e.CasePhaseId).HasColumnName("CasePhaseID");

                entity.Property(e => e.CaseStatusId).HasColumnName("CaseStatusID");

                entity.Property(e => e.CaseStatusReasonId).HasColumnName("CaseStatusReasonID");

                entity.Property(e => e.ClientNet).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CombinedName)
                    .HasColumnName("combined name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPerson)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Dateentered)
                    .HasColumnName("dateentered")
                    .HasColumnType("datetime");

                entity.Property(e => e.ECourts).HasColumnName("eCourts");

                entity.Property(e => e.Emailed)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Enteredby)
                    .HasColumnName("enteredby")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.EstPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.Faxed)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Firmhistoryid).HasColumnName("firmhistoryid");

                entity.Property(e => e.FollowupDate).HasColumnType("datetime");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.LastUpdateReceived).HasColumnType("datetime");

                entity.Property(e => e.Liens).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MedicalLiens).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NoPortfolioId).HasColumnName("NoPortfolioID");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.PayoffAmt1).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PayoffAmt2).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PayoffDate1).HasColumnType("datetime");

                entity.Property(e => e.PayoffDate2).HasColumnType("datetime");

                entity.Property(e => e.PoContactType)
                    .HasColumnName("PO_ContactType")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PoDaysOut).HasColumnName("PO_DaysOut");

                entity.Property(e => e.PoPriority)
                    .HasColumnName("PO_Priority")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PoReq).HasColumnName("PO_Req");

                entity.Property(e => e.PoRequestedBy)
                    .HasColumnName("PO_RequestedBy")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PoRequestersName)
                    .HasColumnName("PO_RequestersName")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PoType)
                    .HasColumnName("PO_Type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PortfolioId).HasColumnName("PortfolioID");

                entity.Property(e => e.Reason)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RedApprovalDate)
                    .HasColumnName("Red_ApprovalDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.RedApprovedAmt)
                    .HasColumnName("Red_ApprovedAmt")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RedReqApprovedById).HasColumnName("Red_ReqApprovedById");

                entity.Property(e => e.RedReqNegotiatedById).HasColumnName("Red_ReqNegotiatedById");

                entity.Property(e => e.ReductionReq).HasColumnName("Reduction_Req");

                entity.Property(e => e.SettledFor).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SettledForDate).HasColumnType("datetime");

                entity.Property(e => e.Source)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusCategory)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubpoenaInfo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SuggestedAmt).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<VwUserActivity>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwUserActivity");

                entity.Property(e => e.ActionName).HasMaxLength(4000);

                entity.Property(e => e.MachineName).HasMaxLength(4000);

                entity.Property(e => e.UserName).HasMaxLength(4000);

                entity.Property(e => e.VisitDate).HasColumnType("datetime");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
