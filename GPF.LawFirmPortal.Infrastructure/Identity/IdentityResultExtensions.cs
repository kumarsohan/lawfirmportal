﻿using System.Linq;
using GPF.LawFirmPortal.Application.Common.Models;
using Microsoft.AspNetCore.Identity;

namespace GPF.LawFirmPortal.Infrastructure.Identity
{
    public static class IdentityResultExtensions
    {
        public static Result ToApplicationResult(this IdentityResult result)
        {
            return result.Succeeded
                ? Result.Success()
                : Result.Failure(result.Errors.Select(e => e.Description));
        }
    }
}
