﻿using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Infrastructure.Identity;
using GPF.LawFirmPortal.Infrastructure.Repositories;
using GPF.LawFirmPortal.Infrastructure.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace GPF.LawFirmPortal.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(
            this IServiceCollection services,
            IConfiguration configuration,
            IWebHostEnvironment environment)
        {
            services.AddDbContext<GPFLawFirmPortalContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<GPFSystemContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("GPFSystemConnection")));

            services.AddIdentity<IdentityUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddDefaultTokenProviders()
                 .AddEntityFrameworkStores<GPFLawFirmPortalContext>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddMvc();

            // Add application services.
            services.AddTransient<IEmailSender, NotificationService>();
            //services.AddTransient<ISmsSender, NotificationService>();

            //services.AddTransient<IMenuService, MenuService>();
            services.AddTransient<ILoggerService, LoggerService>();
            services.AddTransient<ILdapService, LdapService>();
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<ICommonService, CommonService>();
            services.AddSingleton<IUserConnectionManager, UserConnectionManager>();
            services.AddTransient<IPushService, PushService>();
            return services;
        }
    }
}
