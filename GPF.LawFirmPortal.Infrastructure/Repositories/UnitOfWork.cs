﻿using GPF.LawFirmPortal.Application.Common.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ICurrentUserService _currentUserService;
        private GPFLawFirmPortalContext _dbContext;
        private GPFSystemContext _dbGPFSystemContext;

        public UnitOfWork(GPFLawFirmPortalContext context, ICurrentUserService currentUserService, GPFSystemContext GPFSystemContext)
        {
            _dbContext = context;
            _currentUserService = currentUserService;
            _dbGPFSystemContext = GPFSystemContext;
        }

        public void Add<T>(T obj)
            where T : class
        {
            obj = this.SetColumns(obj, "add", _currentUserService.UserName);
            var set = _dbContext.Set<T>();
            set.Add(obj);
        }

        public T GetById<T>(int? id)
            where T : class
        {
            return _dbContext.Set<T>().Find(id);
        }

        public T AddObj<T>(T obj)
            where T : class
        {
            obj = this.SetColumns(obj, "add", _currentUserService.UserName);
            var set = _dbContext.Set<T>();
            set.Add(obj);
            //_dbContext.Entry(obj).State = EntityState.Added;
            return obj;
        }

        public void Update<T>(T obj)
            where T : class
        {
            obj = this.SetColumns(obj, "update", _currentUserService.UserName);
            var set = _dbContext.Set<T>();
            set.Attach(obj);
            _dbContext.Entry(obj).State = EntityState.Modified;
        }

        void IUnitOfWork.Remove<T>(T obj)
        {
            var set = _dbContext.Set<T>();
            set.Remove(obj);
        }

        public IQueryable<T> Query<T>()
            where T : class
        {
            return _dbContext.Set<T>();
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public async Task CommitAsync(CancellationToken cancellationToken)
        {
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public void Attach<T>(T newUser) where T : class
        {
            var set = _dbContext.Set<T>();
            set.Attach(newUser);
        }



        public void AddGPFSystem<T>(T obj)
            where T : class
        {
            obj = this.SetColumns(obj, "add", _currentUserService.UserName);
            var set = _dbGPFSystemContext.Set<T>();
            set.Add(obj);
        }

        public T GetByIdGPFSystem<T>(int? id)
            where T : class
        {
            return _dbGPFSystemContext.Set<T>().Find(id);
        }

        public T AddObjGPFSystem<T>(T obj)
            where T : class
        {
            obj = this.SetColumns(obj, "add", _currentUserService.UserName);
            var set = _dbGPFSystemContext.Set<T>();
            set.Add(obj);
            //_dbContext.Entry(obj).State = EntityState.Added;
            return obj;
        }

        public void UpdateGPFSystem<T>(T obj)
            where T : class
        {
            obj = this.SetColumns(obj, "update", _currentUserService.UserName);
            var set = _dbGPFSystemContext.Set<T>();
            set.Attach(obj);
            _dbContext.Entry(obj).State = EntityState.Modified;
        }

        void IUnitOfWork.RemoveGPFSystem<T>(T obj)
        {
            var set = _dbGPFSystemContext.Set<T>();
            set.Remove(obj);
        }

        public IQueryable<T> QueryGPFSystem<T>()
            where T : class
        {
            return _dbGPFSystemContext.Set<T>();
        }

        public void CommitGPFSystem()
        {
            _dbGPFSystemContext.SaveChanges();
        }

        public async Task CommitAsyncGPFSystem(CancellationToken cancellationToken)
        {
            await _dbGPFSystemContext.SaveChangesAsync(cancellationToken);
        }

        public void AttachGPFSystem<T>(T newUser) where T : class
        {
            var set = _dbGPFSystemContext.Set<T>();
            set.Attach(newUser);
        }

        public void Dispose()
        {
            _dbContext = null;
            _dbGPFSystemContext = null;
        }

        private T SetColumns<T>(T entity, string action, string username)
            where T : class
        {
            var type = entity.GetType();

            switch (action.ToLower())
            {
                case "add":
                    {
                        var propCreated = type.GetProperty("DateCreated");
                        if (propCreated != null)
                        {
                            propCreated.SetValue(entity, DateTime.Now);
                            propCreated = type.GetProperty("CreatedBy");
                            if (propCreated != null)
                            {
                                propCreated.SetValue(entity, username);
                            }
                        }

                        break;
                    }

                case "update":
                    {
                        var propModified = type.GetProperty("DateModified");
                        if (propModified != null)
                        {
                            propModified.SetValue(entity, DateTime.Now);
                            propModified = type.GetProperty("ModifiedBy");
                            if (propModified != null)
                            {
                                propModified.SetValue(entity, username);
                            }
                        }

                        break;
                    }
            }

            return entity;
        }

        public IList<T> ExecuteStoredProcedure<T>(DbCommand command)
            where T : class
        {
            using (command)
            {
                if (command.Connection.State == ConnectionState.Closed)
                    command.Connection.Open();
                try
                {
                    using var reader = command.ExecuteReader();
                    return reader.MapToList<T>();
                }
                catch (Exception ex)
                {
                    var test = ex.Message;
                    IList<T> tester = null;
                    return tester;
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        public DbCommand GetStoredProcedure(string name, params (string, object)[] nameValueParams)
        {
            return _dbContext
                .LoadStoredProcedure(name)
                .WithSqlParams(nameValueParams);
        }

        public DbCommand GetStoredProcedure(string name)
        {
            return _dbContext.LoadStoredProcedure(name);
        }

        public static IList<List<T>> MapToListArray<T>(DbDataReader dr, List<T> listArray)
        {
            var objList = new List<T>();
            IList<List<T>> genericResult = new List<List<T>>();
            var props = typeof(T).GetRuntimeProperties();

            var colMapping = dr.GetColumnSchema()
                .Where(x => props.Any(y =>
                    string.Equals(y.Name, x.ColumnName, StringComparison.OrdinalIgnoreCase)))
                .ToDictionary(key => key.ColumnName.ToLower(CultureInfo.InvariantCulture));

            if (!dr.HasRows) return genericResult;
            do
            {
                while (dr.Read())
                {
                    T obj = Activator.CreateInstance<T>();
                    foreach (var prop in props)
                    {
                        var columnOrdinal = colMapping[prop.Name.ToLower(CultureInfo.InvariantCulture)].ColumnOrdinal;
                        if (columnOrdinal == null) continue;
                        var val = dr.GetValue(columnOrdinal.Value);
                        prop.SetValue(obj, val == DBNull.Value ? null : val);
                    }
                    objList.Add(obj);
                }

                genericResult.Add(objList);
                objList = new List<T>();

            }
            while (dr.NextResult());

            return genericResult;
        }

        public dynamic ExecuteMultipleSetProc(DbCommand command, List<string> multipleClasses)
        {
            var listType = typeof(List<>);
            List<IList> resultSet = new List<IList>();

            using (command)
            {
                if (command.Connection.State == ConnectionState.Closed)
                    command.Connection.Open();
                try
                {
                    using var dr = command.ExecuteReader();
                    int index = 0;
                    do
                    {
                        Type type = Type.GetType(multipleClasses[index]);
                        var constructedListType = listType.MakeGenericType(type);
                        var objList = (IList)Activator.CreateInstance(constructedListType);

                        while (dr.Read())
                        {

                            var obj = Activator.CreateInstance(type);

                            var props = type.GetRuntimeProperties();

                            var colMapping = dr.GetColumnSchema()
                                .Where(x => props.Any(y =>
                                    string.Equals(y.Name, x.ColumnName, StringComparison.OrdinalIgnoreCase)))
                                .ToDictionary(key => key.ColumnName.ToLower());

                            foreach (var prop in props)
                            {
                                var columnOrdinal = colMapping[prop.Name.ToLower()].ColumnOrdinal;
                                if (columnOrdinal == null) continue;
                                var val = dr.GetValue(columnOrdinal.Value);
                                prop.SetValue(obj, val == DBNull.Value ? null : val);
                            }
                            objList.Add(obj);
                        }

                        resultSet.Add(objList);
                        index++;
                    }
                    while (dr.NextResult());

                    return resultSet;
                }
                catch (Exception ex)
                {
                    var test = ex.Message;
                    return null;
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
    }

    public static class SprocRepositoryExtensions
    {
        public static DbCommand LoadStoredProcedure(this GPFLawFirmPortalContext context, string storedProcName)
        {
            var cmd = context.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = storedProcName;
            cmd.CommandType = CommandType.StoredProcedure;
            return cmd;
        }

        public static DbCommand WithSqlParams(this DbCommand cmd, params (string, object)[] nameValueParamPairs)
        {
            foreach (var pair in nameValueParamPairs)
            {
                var param = cmd.CreateParameter();
                param.ParameterName = pair.Item1;
                param.Value = pair.Item2 ?? DBNull.Value;
                cmd.Parameters.Add(param);
            }

            return cmd;
        }

        public static IList<T> MapToList<T>(this DbDataReader dr)
        {
            var objList = new List<T>();
            var props = typeof(T).GetRuntimeProperties();

            var colMapping = dr.GetColumnSchema()
                .Where(x => props.Any(y =>
                    string.Equals(y.Name, x.ColumnName, StringComparison.OrdinalIgnoreCase)))
                .ToDictionary(key => key.ColumnName.ToLower());

            if (!dr.HasRows) return objList;
            while (dr.Read())
            {
                T obj = Activator.CreateInstance<T>();
                foreach (var prop in props)
                {
                    System.Diagnostics.Debug.WriteLine(prop);
                    var columnOrdinal = colMapping[prop.Name.ToLower()].ColumnOrdinal;
                    if (columnOrdinal == null) continue;
                    var val = dr.GetValue(columnOrdinal.Value);
                    prop.SetValue(obj, val == DBNull.Value ? null : val);
                }

                objList.Add(obj);
            }

            return objList;
        }
    }
}
