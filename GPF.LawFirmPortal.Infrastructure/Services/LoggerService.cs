﻿using System;
using System.Threading.Tasks;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Domain.Entities;

namespace GPF.LawFirmPortal.Infrastructure.Services
{
    public class LoggerService : ILoggerService
    {
        private readonly IUnitOfWork _unitOfWork;

        public LoggerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int LogInfo(string userId, string cat = "", string message = "", string trace = "")
        {
            try
            {
                //AuditLog auditLog = new AuditLog
                //{
                //    Action = trace,
                //    Email = userId,
                //    Date = DateTime.Now,
                //};

                //_unitOfWork.Add(auditLog);
                //_unitOfWork.Commit();
                //return auditLog.AuditLogId;
                return 1;
            }
            catch (Exception e)
            {
                return 0;
                // Todo Write into file
            }
        }

        //public int LogError(string userId, string cat = "", string message = "", string trace = "")
        //{
        //    try
        //    {
        //        ErrorLog error = new ErrorLog
        //        {
        //            Action = message,
        //            Email = userId,
        //            Date = DateTime.Now,
        //        };

        //        _unitOfWork.Add(error);
        //        _unitOfWork.Commit();
        //        return error.ErrorLogId;
        //    }
        //    catch (Exception e)
        //    {
        //        return 0;
        //        // Todo Write into file
        //    }
        //}

    }
}
