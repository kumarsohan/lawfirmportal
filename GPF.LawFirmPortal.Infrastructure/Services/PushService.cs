﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebPush;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using PushSubscription = GPF.LawFirmPortal.Application.Common.Models.PushSubscription;

namespace GPF.LawFirmPortal.Infrastructure.Services
{
    /// <inheritdoc />
    public class PushService : IPushService
    {
        private readonly WebPushClient _client;
        private readonly IUnitOfWork _unitOfWork;
        private readonly VapidDetails _vapidDetails;

        /// <inheritdoc />
        public PushService(IUnitOfWork unitOfWork, string vapidSubject, string vapidPublicKey, string vapidPrivateKey)
        {
            _unitOfWork = unitOfWork;
            _client = new WebPushClient();

            CheckOrGenerateVapidDetails(vapidSubject, vapidPublicKey, vapidPrivateKey);

            _vapidDetails = new VapidDetails(vapidSubject, vapidPublicKey, vapidPrivateKey);
        }

        /// <inheritdoc />
        public PushService(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _client = new WebPushClient();

            var vapidSubject = configuration.GetValue<string>("Vapid:Subject");
            var vapidPublicKey = configuration.GetValue<string>("Vapid:PublicKey");
            var vapidPrivateKey = configuration.GetValue<string>("Vapid:PrivateKey");

            CheckOrGenerateVapidDetails(vapidSubject, vapidPublicKey, vapidPrivateKey);

            _vapidDetails = new VapidDetails(vapidSubject, vapidPublicKey, vapidPrivateKey);
        }

        /// <inheritdoc />
        public void CheckOrGenerateVapidDetails(string vapidSubject, string vapidPublicKey, string vapidPrivateKey)
        {
            if (string.IsNullOrEmpty(vapidSubject) || 
                string.IsNullOrEmpty(vapidPublicKey) ||
                string.IsNullOrEmpty(vapidPrivateKey))
            {
                var vapidKeys = VapidHelper.GenerateVapidKeys();

                // Prints 2 URL Safe Base64 Encoded Strings
                Debug.WriteLine($"Public {vapidKeys.PublicKey}");
                Debug.WriteLine($"Private {vapidKeys.PrivateKey}");

                throw new Exception(
                    "You must set the Vapid:Subject, Vapid:PublicKey and Vapid:PrivateKey application settings or pass them to the service in the constructor. You can use the ones just printed to the debug console.");
            }
        }

        /// <inheritdoc />
        public string GetVapidPublicKey() => _vapidDetails.PublicKey;

        /// <inheritdoc />
        public async Task<PushSubscription> Subscribe(PushSubscription subscription)
        {
            //if (await _context.PushSubscription.AnyAsync(s => s.P256Dh == subscription.P256Dh))
            //    return await _context.PushSubscription.FindAsync(subscription.P256Dh);

            if (await _unitOfWork.Query<Domain.Entities.PushSubscription>().AnyAsync(s => s.P256dh == subscription.P256Dh))
            {
                var subscriptionObj = _unitOfWork.Query<Domain.Entities.PushSubscription>().FirstOrDefault(f => f.P256dh == subscription.P256Dh);
                return new PushSubscription()
                {
                    Auth = subscriptionObj.Auth,
                    Endpoint = subscriptionObj.Endpoint,
                    ExpirationTime = subscriptionObj.ExpirationTime,
                    P256Dh = subscriptionObj.P256dh,
                    UserId = subscriptionObj.UserId
                };
            }
            _unitOfWork.Add<Domain.Entities.PushSubscription>(new Domain.Entities.PushSubscription()
            {
                P256dh = subscription.P256Dh,
                UserId = subscription.UserId,
                ExpirationTime = subscription.ExpirationTime,
                Endpoint = subscription.Endpoint,
                Auth = subscription.Auth
            });
            //await _context.PushSubscription.AddAsync(subscription);
            await _unitOfWork.CommitAsync(cancellationToken: default).ConfigureAwait(true);

            return subscription;
        }

        /// <inheritdoc />
        public async Task Unsubscribe(PushSubscription subscription)
        {
            if (!await _unitOfWork.Query<Domain.Entities.PushSubscription>().AnyAsync(s => s.P256dh == subscription.P256Dh)) return;
            var subscriptionObj = _unitOfWork.Query<Domain.Entities.PushSubscription>().FirstOrDefault(f => f.P256dh == subscription.P256Dh);
            _unitOfWork.Remove(subscriptionObj);
            await _unitOfWork.CommitAsync(cancellationToken: default).ConfigureAwait(true); ;
        }

        /// <inheritdoc />
        public async Task Send(string userId, WebPushNotification notification)
        {
            userId = "LawFirmPortalAdmin";
            foreach (var subscription in await GetUserSubscriptions(userId))
                try
                {
                    _client.SendNotification(subscription.ToWebPushSubscription(), JsonConvert.SerializeObject(notification), _vapidDetails);
                }
                catch (WebPushException e)
                {
                    if (e.Message == "Subscription no longer valid")
                    {
                        var pushsubscription = new PushSubscription()
                        {
                            P256Dh = subscription.P256Dh,
                            UserId = subscription.UserId,
                            ExpirationTime = subscription.ExpirationTime,
                            Endpoint = subscription.Endpoint,
                            Auth = subscription.Auth
                        };
                        await Unsubscribe(pushsubscription);
                    }
                    else
                    {
                        // Track exception with eg. AppInsights
                    }
                }
        }

        /// <inheritdoc />
        public async Task Send(string userId, string text)
        {
            await Send(userId, new WebPushNotification(text));
        }

        /// <summary>
        /// Loads a list of user subscriptions from the database
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>List of subscriptions</returns>
        private async Task<List<PushSubscription>> GetUserSubscriptions(string userId) =>
            await _unitOfWork.Query<Domain.Entities.PushSubscription>().Where(s => s.UserId == userId).Select(s => new PushSubscription()
            {
                Auth = s.Auth,
                Endpoint = s.Endpoint,
                ExpirationTime = s.ExpirationTime,
                P256Dh = s.P256dh,
                UserId = s.UserId
            }).ToListAsync();
    }
}
