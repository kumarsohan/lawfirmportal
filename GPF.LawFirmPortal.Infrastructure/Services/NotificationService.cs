﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Notifications.Models;
using GPF.LawFirmPortal.Domain.Entities;
using Microsoft.Extensions.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using static GPF.LawFirmPortal.Application.Common.Utility.AppConstants;

namespace GPF.LawFirmPortal.Infrastructure.Services
{
    public class NotificationService : IEmailSender
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;

        public NotificationService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        public Task SendEmailAsync(MessageDto message, byte[] fileBytes = null, Dictionary<string, string> placeHolders = null, string fileName = null)
        {
            var sentFrom = _configuration[EmailSettings.FROMEMAIL];
            var displayName = _configuration[EmailSettings.FROMNAME];
            var pwd = _configuration[EmailSettings.PASSWORD];

            // Configure the client:
            SmtpClient client = new SmtpClient
            {
                Host = _configuration[EmailSettings.HOST],
                Port = int.Parse(_configuration[EmailSettings.PORT], CultureInfo.InvariantCulture),
                EnableSsl = _configuration[EmailSettings.ENABLESSL] == "1" ? true : false,
                UseDefaultCredentials = false,
            };

            if (client.EnableSsl)
            {
                client.Credentials = new NetworkCredential(sentFrom, pwd);
            }

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(sentFrom, displayName);

            foreach (var address in message.To.Split(";", StringSplitOptions.RemoveEmptyEntries))
            {
                mail.To.Add(address);
            }

            var emailSubjectAndBody = GetEmailSubjectAndBody(message.TemplateId, placeHolders, message);
            mail.Subject = emailSubjectAndBody.Subject;
            mail.Body = emailSubjectAndBody.Body;
            if (!string.IsNullOrEmpty(message.CC))
            {
                mail.CC.Add(message.CC);
            }

            mail.IsBodyHtml = true;

            if (fileBytes != null)
            {
                mail.Attachments.Add(GetAttachment(fileBytes, fileName));
            }

            // Send:
            return client.SendMailAsync(mail);
        }

        public Task SendSmsAsync(MessageDto message)
        {
             var accountSid = _configuration[SMSSettings.ACCOUNTSID];
             var authToken = _configuration[SMSSettings.AUTHTOKEN];

             TwilioClient.Init(accountSid, authToken);

             return MessageResource.CreateAsync(
                 to: new PhoneNumber(message.To),
                 from: new PhoneNumber(_configuration[SMSSettings.FROM]),
                 body: message.Body);
        }

        private Attachment GetAttachment(byte[] fileBytes, string fileName)
        {
            MemoryStream ms = new MemoryStream(fileBytes, 0, fileBytes.Length);
            Attachment attachment = new Attachment(ms, MediaTypeNames.Application.Pdf);
            attachment.TransferEncoding = TransferEncoding.Base64;
            ContentDisposition disposition = attachment.ContentDisposition;
            disposition.DispositionType = DispositionTypeNames.Attachment;
            disposition.FileName = $"{fileName}.pdf";
            return attachment;
        }

        private MessageDto GetEmailSubjectAndBody(int templateId, Dictionary<string, string> mailInfo, MessageDto message)
        {
            var emailTemplate = _unitOfWork.GetById<EmailTemplate>(templateId);
            string mailSubject = emailTemplate.Subject;
            string mailBody = emailTemplate.Message;
            foreach (KeyValuePair<string, string> pair in mailInfo)
            {
                mailBody = mailBody.Replace("$" + pair.Key + "$", pair.Value, StringComparison.InvariantCulture);
                mailSubject = mailSubject.Replace("$" + pair.Key + "$", pair.Value, StringComparison.InvariantCulture);
            }

            message.Body = mailBody;
            message.Subject = mailSubject;
            return message;
        }
    }
}
