﻿using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPF.LawFirmPortal.Infrastructure.Services
{
    public class CommonService : ICommonService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CommonService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public string GetLawFirmName(int LawFirmId)
        {
            var lawFirm = _unitOfWork.Query<VwLawFirm>().FirstOrDefault(l => l.LawFirmId == LawFirmId);
            if (lawFirm != null)
            {
                return lawFirm.Name;
            }
            return "";
        }
    }
}
