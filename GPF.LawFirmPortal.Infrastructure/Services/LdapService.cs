﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Models;
using Microsoft.Extensions.Configuration;
using static GPF.LawFirmPortal.Application.Common.Utility.AppConstants;

namespace GPF.LawFirmPortal.Infrastructure.Services
{
    public class LdapService : ILdapService
    {
        private readonly string ldapIP;
        private readonly int ldapPort;
        private readonly string ldapPath;
        private readonly string[] ValidLoginGroups;

        private readonly IConfiguration _configuration;

        public LdapService(IConfiguration configuration)
        {
            _configuration = configuration;
            ldapIP = _configuration[LDAPSettings.LDAPIP];
            ldapPort = Convert.ToInt32(_configuration[LDAPSettings.LDAPPORT]);
            ldapPath = _configuration[LDAPSettings.LDAPPATH];
            ValidLoginGroups= _configuration[LDAPSettings.VALIDLOGINGROUPS].Split(",");
        }

        public bool ValidateCredentials(string userName, string password)
        {
            bool flag = false;
            try
            {
                LdapDirectoryIdentifier ldi = new LdapDirectoryIdentifier(ldapIP, ldapPort);
                LdapConnection ldapConnection = new LdapConnection(ldi);
                ldapConnection.AuthType = AuthType.Basic;
                ldapConnection.SessionOptions.ProtocolVersion = 3;
                NetworkCredential nc = new NetworkCredential(userName, password);
                ldapConnection.Bind(nc);
                ldapConnection.Dispose();
                flag = true;
            }
            catch (Exception)
            {
                flag = false;
            }

            return flag;
        }
        public LdapUser ValidateADDirectory(string userName, string password)
        {
            var UserName = GetADUserByEmail(userName);
            string lawFirmIdAttr = string.Empty;
            List<string> lstGroupNames = new List<string>();
            try
            {
                using (DirectoryEntry entry = new DirectoryEntry(this.ldapPath, UserName,password))
                {
                    using (DirectorySearcher searcher = new DirectorySearcher(entry))
                    {
                        searcher.Filter = String.Format("({0}={1})", "SAMAccountName", UserName);
                        var user = searcher.FindOne();                        
                        if (user != null)
                        {
                            var accountNameAttr = user.Properties["sAMAccountName"][0].ToString();
                            if (accountNameAttr == null)
                            {
                                throw new Exception("Your account is missing the account name.");
                            }

                            var displayNameAttr = user.Properties["displayname"][0].ToString();
                            if (displayNameAttr == null)
                            {
                                throw new Exception("Your account is missing the display name.");
                            }

                            var emailAttr = user.Properties["mail"][0].ToString();
                            if (emailAttr == null)
                            {
                                throw new Exception("Your account is missing an email.");
                            }
                            if (user.Properties["phsfirmid"].Count != 0)
                            {
                                lawFirmIdAttr = user.Properties["phsfirmid"][0].ToString();
                                if (lawFirmIdAttr == null)
                                {
                                    throw new Exception("Your account is missing an email.");
                                }
                            }

                            var memberAttr = user.Properties["memberOf"].ToString();
                            if (memberAttr == null)
                            {
                                throw new Exception("Your account is missing roles.");
                            }
                            foreach (string item in user.Properties["memberof"])
                            {
                                lstGroupNames.Add(GetGroup(item));
                            }
                            var isUserExists = this.ValidLoginGroups.Any(s => lstGroupNames.Contains(s));

                            if (isUserExists == false)
                                return null;
                            return new LdapUser
                            {
                                DisplayName = displayNameAttr,
                                Username = accountNameAttr,
                                Email = emailAttr,
                                LawFirmID = lawFirmIdAttr,
                                GroupName = lstGroupNames.Where(s => this.ValidLoginGroups.Contains(s)).FirstOrDefault()
                            };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return null;            
        }
        private string GetGroup(string value)
        {
            Match match = Regex.Match(value, "^CN=([^,]*)");
            if (!match.Success)
            {
                return null;
            }

            return match.Groups[1].Value;
        }
        public string GetADUserByEmail(string emailId)
        {
            var strSamAccountName = String.Empty;
            try
            {
                DirectoryEntry searchRoot = new DirectoryEntry(this.ldapPath);
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                if (!string.IsNullOrEmpty(emailId))
                    search.Filter = ("mail=" + emailId);
                else
                    search.Filter = "(&(objectClass=user)(objectCategory=person))";
                search.SizeLimit = 10;

                SearchResult resultCol = search.FindOne();

                if (resultCol != null)
                {
                    strSamAccountName= resultCol.Properties["sAMAccountName"][0].ToString();
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strSamAccountName;
        }
    }
}
