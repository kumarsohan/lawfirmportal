﻿using System;
using Microsoft.EntityFrameworkCore;
using GPF.LawFirmPortal.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata;

namespace GPF.LawFirmPortal.Infrastructure
{
    public partial class GPFSystemContext : DbContext
    {
        public GPFSystemContext()
        {
        }

        public GPFSystemContext(DbContextOptions<GPFSystemContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FollowUpRules> FollowUpRules { get; set; }
        public virtual DbSet<StatusEntries> StatusEntries { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("server=WIN-DEV-SQL01;user id=DataWriter2019;password=DwGPF2019;database=GPFSystem");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FollowUpRules>(entity =>
            {
                entity.HasKey(e => e.FollowUpId);

                entity.HasIndex(e => e.CappedAmountMax);

                entity.HasIndex(e => e.CappedAmountMin);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.SubStatusId);

                entity.HasIndex(e => new { e.CappedAmountMax, e.CappedAmountMin })
                    .HasName("IX_FollowUpRules_CappedAmount");

                entity.HasIndex(e => new { e.StatusId, e.SubStatusId })
                    .HasName("IX_FollowUpRules_StatusSubStatusId");

                entity.Property(e => e.FollowUpId).HasColumnName("FollowUpID");

                entity.Property(e => e.CappedAmountMax).HasColumnType("decimal(14, 2)");

                entity.Property(e => e.CappedAmountMin).HasColumnType("decimal(14, 2)");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.SubStatusId).HasColumnName("SubStatusID");
            });

            modelBuilder.Entity<StatusEntries>(entity =>
            {
                entity.HasIndex(e => e.CombinedName)
                    .HasName("IX_tblGPStatusEntries");

                entity.HasIndex(e => e.FromPyrus)
                    .HasName("IX_StatusEntries");

                entity.HasIndex(e => e.PortfolioId)
                    .HasName("IX_tblGPStatusEntries_1");

                entity.HasIndex(e => new { e.CaseId, e.FromPyrus })
                    .HasName("ix1");

                entity.HasIndex(e => new { e.ContactPerson, e.Emailed, e.CaseId })
                    .HasName("IX_StatusEntries_CaseID");

                entity.HasIndex(e => new { e.Id, e.CombinedName, e.StatusCategory })
                    .HasName("IX_StatusEntries_StatusCategory");

                entity.HasIndex(e => new { e.Id, e.PortfolioId, e.NoPortfolioId })
                    .HasName("IX_StatusEntries_NoPortfolioID");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AttorneyFee).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Called)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CaseContactStatusId).HasColumnName("CaseContactStatusID");

                entity.Property(e => e.CaseId).HasColumnName("CaseID");

                entity.Property(e => e.CasePhaseId).HasColumnName("CasePhaseID");

                entity.Property(e => e.CaseStatusId).HasColumnName("CaseStatusID");

                entity.Property(e => e.CaseStatusReasonId).HasColumnName("CaseStatusReasonID");

                entity.Property(e => e.ClientNet).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CombinedName)
                    .HasColumnName("combined name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ContactAttempted).HasDefaultValueSql("((0))");

                entity.Property(e => e.ContactPerson)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Dateentered)
                    .HasColumnName("dateentered")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ECourts)
                    .HasColumnName("eCourts")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Emailed)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Enteredby)
                    .HasColumnName("enteredby")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.EstPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.Faxed)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Firmhistoryid).HasColumnName("firmhistoryid");

                entity.Property(e => e.FollowupDate).HasColumnType("datetime");

                entity.Property(e => e.FromPyrus).HasDefaultValueSql("((0))");

                entity.Property(e => e.InPerson).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastUpdateReceived).HasColumnType("datetime");

                entity.Property(e => e.Liens).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MailCorrespondance).HasDefaultValueSql("((0))");

                entity.Property(e => e.MedicalLiens).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NoPortfolioId)
                    .HasColumnName("NoPortfolioID")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NoStatusChange).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.PayoffAmt1).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PayoffAmt2).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PayoffDate1).HasColumnType("datetime");

                entity.Property(e => e.PayoffDate2).HasColumnType("datetime");

                entity.Property(e => e.PoContactType)
                    .HasColumnName("PO_ContactType")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PoDaysOut).HasColumnName("PO_DaysOut");

                entity.Property(e => e.PoPriority)
                    .HasColumnName("PO_Priority")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PoReq)
                    .HasColumnName("PO_Req")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PoRequestedBy)
                    .HasColumnName("PO_RequestedBy")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PoRequestersName)
                    .HasColumnName("PO_RequestersName")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PoType)
                    .HasColumnName("PO_Type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PortfolioId).HasColumnName("PortfolioID");

                entity.Property(e => e.Reason)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RedApprovalDate)
                    .HasColumnName("Red_ApprovalDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.RedApprovedAmt)
                    .HasColumnName("Red_ApprovedAmt")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RedReqApprovedById).HasColumnName("Red_ReqApprovedById");

                entity.Property(e => e.RedReqNegotiatedById).HasColumnName("Red_ReqNegotiatedById");

                entity.Property(e => e.ReductionReq)
                    .HasColumnName("Reduction_Req")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SettledFor).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SettledForDate).HasColumnType("datetime");

                entity.Property(e => e.Source)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Good')");

                entity.Property(e => e.StatusCategory)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Regular Follow-up')");

                entity.Property(e => e.SubpoenaInfo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SuggestedAmt).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => new { e.UserId, e.RecordStatus, e.UserName, e.Email, e.DisplayName, e.Active })
                    .HasName("idx_Nonclustered_User_Filtering");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Active).HasDefaultValueSql("((1))");

                entity.Property(e => e.Appversion)
                    .HasColumnName("appversion")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AutoAdjustUac)
                    .HasColumnName("autoAdjustUAC")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CanExportAllDocs)
                    .HasColumnName("canExportAllDocs")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CanGenPo)
                    .HasColumnName("canGenPO")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CanMakeAdminChanges)
                    .HasColumnName("canMakeAdminChanges")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CanMarkPosent)
                    .HasColumnName("canMarkPOSent")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CanViewDocAudit)
                    .HasColumnName("canViewDocAudit")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CanViewRiskMgt)
                    .HasColumnName("canViewRiskMgt")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ComplianceView).HasDefaultValueSql("((0))");

                entity.Property(e => e.Computername)
                    .HasColumnName("computername")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Dateloggedin)
                    .HasColumnName("dateloggedin")
                    .HasColumnType("datetime");

                entity.Property(e => e.DisplayName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsAdmin)
                    .HasColumnName("isAdmin")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsReadOnly)
                    .HasColumnName("isReadOnly")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LastName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LegalUser)
                    .HasColumnName("legalUser")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LegalViewRoles)
                    .HasColumnName("LegalView_Roles")
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RecordStatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.SpFilter)
                    .HasColumnName("sp_filter")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.SpUser1)
                    .HasColumnName("sp_User1")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.InverseCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_User_CreatedBy");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.InverseModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_User_ModifiedBy");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
