﻿using System.Security.Claims;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using Microsoft.AspNetCore.Http;

namespace GPF.LawFirmPortal.Web.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private string _sessionUserName;

        public CurrentUserService()
        {
            _contextAccessor = new HttpContextAccessor();
        }

        public string UserName
        {
            get
            {
                if (string.IsNullOrEmpty(_sessionUserName))
                {
                    if (_contextAccessor.HttpContext != null)
                    {
                        var name = ((ClaimsIdentity)_contextAccessor.HttpContext.User.Identity).Name;
                        _sessionUserName = name;
                    }
                }

                return _sessionUserName;
            }

            set
            {
                _sessionUserName = value;
            }
        }

    }
}
