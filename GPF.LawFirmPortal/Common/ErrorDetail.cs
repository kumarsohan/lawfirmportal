﻿using Newtonsoft.Json;

namespace GPF.LawFirmPortal.Web.Common
{
    public class ErrorDetail
    {
        public int Status { get; set; }

        public string Message { get; set; }

        public bool Success { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
