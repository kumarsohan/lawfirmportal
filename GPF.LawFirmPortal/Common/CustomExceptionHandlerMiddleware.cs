﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using GPF.LawFirmPortal.Application.Common.Exceptions;
using GPF.LawFirmPortal.Domain.Entities;
using GPF.LawFirmPortal.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;

namespace GPF.LawFirmPortal.Web.Common
{
    public class CustomExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        GPFLawFirmPortalContext db = new GPFLawFirmPortalContext();

        public CustomExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError;
            bool redirectToError = false;
            // var isAjaxRequest = context.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            ErrorDetail result = new ErrorDetail();

            switch (exception)
            {
                case ValidationException validationException:
                    code = HttpStatusCode.BadRequest;
                    result.Message = JsonConvert.SerializeObject(validationException.Failures);
                    result.Status = (int)code;
                    result.Success = false;
                    break;
                case NotFoundException _:
                    code = HttpStatusCode.NotFound;
                    result.Message = exception.Message;
                    result.Status = (int)code;
                    result.Success = false;
                    break;
                default:
                    redirectToError = true;
                    result.Message = exception.Message;
                    result.Status = (int)code;
                    result.Success = false;
                    break;
            }
          
            //ErrorLog error = new ErrorLog
            //{
            //    Action = exception.ToString(),
            //    Date = DateTime.Now,
            //};

            //var x =db.ErrorLog.Add(error);
            //var y = db.SaveChanges();

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            if (redirectToError)
            {
                //context.Response.Redirect("/Home/Error", true);
            }

            return context.Response.WriteAsync(result.ToString());
        }
    }
}
