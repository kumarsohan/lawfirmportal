﻿using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using GPF.LawFirmPortal.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
namespace GPF.LawFirmPortal.Web.Common
{
    public static class ClaimsExtensions
    {
        private static IHttpContextAccessor httpContextAccessor;
        public static bool IsAdmin { get; set; }
        public static void SetHttpContextAccessor(IHttpContextAccessor accessor)
        {
            httpContextAccessor = accessor;
        }
        public static async Task AddUpdateClaim(this ClaimsPrincipal currentPrincipal, string key, string value)
        {
            var identity = currentPrincipal.Identity as ClaimsIdentity;
            if (identity == null)
            {
                return;
            }

            // check for existing claim and remove it
            var existingClaim = identity.FindFirst(key);
            if (existingClaim != null)
            {
                identity.RemoveClaim(existingClaim);
            }

            // add new claim
            var claim = new Claim(key, value);
            identity.AddClaim(claim);

            // Persist to store
            //var user = await userManager.GetUserAsync(currentPrincipal).ConfigureAwait(true);
            //await userManager.AddClaimAsync(user, claim).ConfigureAwait(true);
            //await signInManager.RefreshSignInAsync(user).ConfigureAwait(true);
        }

        public static string GetClaimValue(this IPrincipal currentPrincipal, string key)
        {
            var identity = currentPrincipal.Identity as ClaimsIdentity;
            if (identity == null)
            {
                return null;
            }

            var claim = identity.Claims.FirstOrDefault(c => c.Type == key);
            return claim == null ? "" : claim.Value;
        }        
    }
}
