'use strict';

importScripts('./lib/common-js/util.js');

self.addEventListener('install', function (event) {
    self.skipWaiting();
});

self.addEventListener('activate', function (event) {
    event.waitUntil(clients.claim());
});

// Respond to a server push with a user notification
self.addEventListener('push', function (event) {
    if (event.data) {
        const { title, lang = 'en', body, data, tag, timestamp, requireInteraction, actions, image } = event.data.json();

        const promiseChain = self.registration.showNotification(title, {
            lang,
            body,
            data,
            requireInteraction,
            tag: tag || undefined,
            timestamp: timestamp ? Date.parse(timestamp) : undefined,
            actions: actions || undefined,
            image: image || undefined,
            badge: '/images/toast-image.png',
            icon: '/images/toast-image.png'
        });

        // Ensure the toast notification is displayed before exiting this function
        event.waitUntil(promiseChain);
    }
});

self.addEventListener('notificationclick', function (event) {
    event.notification.close();
    if (event.action === 'open-action') {
        event.waitUntil(clients.matchAll({
            type: "window"
        }).then(function (clientList) {
            for (var i = 0; i < clientList.length; i++) {
                var client = clientList[i];
                if (client.url == '/' && 'focus' in client)
                    return client.focus();
            }
            if (clients.openWindow)
                return clients.openWindow(event.notification.data.url + 'RiskManagement/RiskManagement/CaseDetail/' + event.notification.data.id);
        }));
    }
    else if (event.action === 'dismiss-action') {
       //clients.openWindow("/messages?reply=" + messageId);
    }
});

self.addEventListener('pushsubscriptionchange', function (event) {
    event.waitUntil(
        Promise.all([
            Promise.resolve(event.oldSubscription ? deleteSubscription(event.oldSubscription) : true),
            Promise.resolve(event.newSubscription ? event.newSubscription : subscribePush(registration))
                .then(function (sub) { return saveSubscription(sub); })
        ])
    );
});
