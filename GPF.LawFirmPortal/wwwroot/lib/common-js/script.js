
function registerServiceWorker() {
    return navigator.serviceWorker.register('../service-worker.js');
}

function resetServiceWorkerAndPush() {
    return navigator.serviceWorker.getRegistration()
        .then(function (registration) {
            if (registration) {
                return registration.unregister();
            }
        })
        .then(function () {
            return registerServiceWorker().then(function (registration) {
                return registerPush();
            });
        });
}

function subscribePushAndUpdateButtons(registration) {
    return subscribePush(registration).then(function (subscription) {
        //updateUnsubscribeButtons();
        isSubscribed = true;
        initialiseUI();
        return subscription;
    });
}

function registerPush() {
    return navigator.serviceWorker.ready
        .then(function (registration) {
            return registration.pushManager.getSubscription().then(function (subscription) {
                if (subscription) {
                    // renew subscription if we're within 5 days of expiration
                    if (subscription.expirationTime && Date.now() > subscription.expirationTime - 432000000) {
                        return unsubscribePush().then(function () {
                            //updateUnsubscribeButtons();                            
                            isSubscribed = false;
                            initialiseUI();
                            return subscribePushAndUpdateButtons(registration);
                        });
                    }

                    return subscription;
                }

                return subscribePushAndUpdateButtons(registration);
            });
        })
        .then(function (subscription) {
            return saveSubscription(subscription);
        });
}

function sendMessage(title, message, delay) {
    const timestamp = new Date().getTime() + 5 * 1000; // now plus 5000ms
    const notification = {
        title: 'Law Firm Portal',
        body: message,
        tag: "push-notification-tag" + timestamp,
        data: {
            url: PyrusURL,
            id: title
        },
        actions: [
            {
                action: 'open-action',
                title: 'Open',
                icon: '/images/link.png'
            },
            {
                action: 'dismiss-action',
                title: 'Dismiss',
                icon: '/images/cancel.png'
            }
        ]
    };

    const userId = localStorage.getItem('userId');
    let apiUrl = baseSiteURL+`send/${userId}`;
    if (delay) apiUrl += `?delay=${delay}`;

    return fetch(apiUrl, {
        method: 'post',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(notification)
    });
}

function getPushSubscription() {
    return navigator.serviceWorker.ready
        .then(function (registration) {
            return registration.pushManager.getSubscription();
        });
}

function unsubscribePush() {
    console.log('unsubscribe');
    return getPushSubscription().then(function (subscription) {
        return subscription.unsubscribe().then(function () {
            isSubscribed = false;
            deleteSubscription(subscription);
            initialiseUI();
        });
    });
}

function updateUnsubscribeButtons() {
    const unsubBtn = document.getElementById('UnSubscribenotification');
    const unsubBtn2 = document.getElementById('Subscribenotification');

    if (!(navigator.serviceWorker && 'PushManager' in window)) {
        console.log('pushmanager not registered');
        return;
    }
    const fnSubscribe = function (event) {
        event.preventDefault();
        registerPush().then(function () {
            updateUnsubscribeButtons();
        });
    };
    const fnUnSubscribe = function (event) {
        event.preventDefault();
        unsubscribePush().then(function () {
            updateUnsubscribeButtons();
        });
    };

    return getPushSubscription()
        .then(function (subscription) {
            if (subscription) {
                $("#Subscribenotification").hide();
                $("#UnSubscribenotification").show();
                unsubBtn.addEventListener('click', fnUnSubscribe);
            } else {
                $("#Subscribenotification").show();
                $("#UnSubscribenotification").hide();
                unsubBtn2.addEventListener('click', fnSubscribe);
            }
        });
}

//document.addEventListener('DOMContentLoaded', function (event) {
//    const pushBtn = document.getElementById('Subscribenotification');
//    if (!(navigator.serviceWorker && 'PushManager' in window)) {
//        return;
//    }
//    registerServiceWorker().then(function () {
//        registerPush().then(function () {
//        });
//    });
//});
$(document).ready(function () {
    if (!(navigator.serviceWorker && 'PushManager' in window)) {
        return;
    }
    registerServiceWorker().then(function () {
        if (Notification.permission === 'denied') {
            console.warn('The user has blocked notifications.');
            $("#Subscribenotification").hide();
            $("#UnSubscribenotification").show();
            return;
        }       
    });
    initialiseUI();
});
function initialiseUI() {
    getPushSubscription()
        .then(function (subscription) {
            isSubscribed = !(subscription === null);
            if (isSubscribed) {
                console.log('User IS subscribed.');
            } else {
                console.log('User is NOT subscribed.');
            }
            updateBtn();
        });
}
$("#btnSubscribe").click(function () {

    if (isSubscribed) {
        unsubscribePush();
    } else {
        registerPush().then(function () {
            initialiseUI();
        });
        //return navigator.serviceWorker.ready
        //    .then(function (registration) {
        //        subscribePushAndUpdateButtons(registration);
        //    });
    }
});
function updateBtn() {
    if (Notification.permission === 'denied') {
        $("#UnSubscribenotification").hide();
        //$("#btnSendPush").prop("disabled", true);
        return;
    }
    if (Notification.permission !== 'granted') {
        $("#Subscribenotification").hide();
        //$("#btnSendPush").prop("disabled", true);
        return;
    }
    if (isSubscribed) {
        $("#Subscribenotification").show();
        $("#UnSubscribenotification").hide();
    } else {
        $("#Subscribenotification").hide();
        $("#UnSubscribenotification").show();
    }
}
