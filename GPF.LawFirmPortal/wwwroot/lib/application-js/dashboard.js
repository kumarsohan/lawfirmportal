﻿
var dashboard = new function () {
    this.actionUrls;
    this.init = function (options) {
        dashboard.actionUrls = options;               
    }
    this.FilterDashboard = function () {
        let request = {};

        var startDate = $("#startDate").val();
        var endDate = $("#endDate").val();
        if (dashboard.actionUrls.IsUserAdmin == "True") {
            var selectedLawFirms = $('#TgbxLawFirms').dxTagBox('instance').option('selectedItems');
            selectedLawFirms = jQuery.map(selectedLawFirms, function (n, i) {
                return (n.Value);
            });
            request = { 'StartDate': startDate, 'EndDate': endDate, 'LawFirms': selectedLawFirms };
        }
        else
            request = { 'StartDate': startDate, 'EndDate': endDate };
        let successFun = function (response) {
            if (response) {
                $("#openCase").text(response.OpenCases);
                $("#caseDueUpdate").text(response.CaseDueUpdate);

                $("#settledCaseCount").text(response.SettledCaseCountTotal);

                if (dashboard.actionUrls.IsUserAdmin == "True")
                    $('#CaseDueForUpdates').dxDataGrid('instance').refresh();

                Common.HideLoader();
            }
            else {
                Common.HideLoader();
            }
        }
        Common.Ajax("Post", dashboard.actionUrls.LoadDashboard, request, successFun);
    }
    this.getCurrDate = function (sp, additionYear) {
        today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //As January is 0.
        var yyyy = additionYear == 0 ? today.getFullYear() : today.getFullYear() - additionYear;

        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        return (mm + sp + dd + sp + yyyy);
    }
    this.RestrictItemsCount = function (e) {
        var maxItems = 5;
        if (e.component.suspendValueChagned) {
            e.component.suspendValueChagned = false;
            Common.DisplayError("You can select only 5 LawFirm's");
            return;
        }
        if (e.value != null) {
            if (e.value.length > maxItems) {
                e.component.suspendValueChagned = true;
                var allPrevValues = e.previousValue;
                e.component.option('value', allPrevValues);
            }
        }
    }
    this.SelectedLawFirmState = function () {
        if (dashboard.actionUrls.IsUserAdmin == "True") {
            var getSelLawfirm = dashboard.actionUrls.LawFirmId;
            if (getSelLawfirm != null && getSelLawfirm != "") {
                var result = getSelLawfirm.split(",").map(x => +x);
                var custom_arr1 = [];
                for (var i = 0; i < result.length; i++) {
                    custom_arr1.push(result[i]);
                }
                $("#TgbxLawFirms").dxTagBox('instance').option('value', custom_arr1);
            }
        }
    }
    this.ClientValueChanged = function (data) {
        let request = {};
        request = { 'PortfolioId': data.itemData.PortfolioId };
        let successFun = function (response) {
            if (response) {
                var form = $("#caseInfoPopup").dxForm().dxForm("instance");
                form.updateData("AmountOwed", response[0].AmountOwed);
                form.updateData("ApprovalAmount", response[0].ApprovalAmount);
                form.updateData("DOB", response[0].DOB);
                form.updateData("DOL", response[0].DOL);
                form.updateData("NoOfDaysLeft", response[0].NoOfDaysLeft);
                form.updateData("PortfolioId", response[0].PortfolioId);
                form.updateData("CaseId", response[0].CaseId);
                Common.HideLoader();
            }
            else {
                Common.HideLoader();
            }
        }
        Common.Ajax("GET", dashboard.actionUrls.LoadClientInfo, request, successFun);
    }
    this.CancelPopup = function () {
        $("#CasePopup").dxPopup("instance").hide();
        $('#caseInfoPopup').dxForm('instance').resetValues();
        $("#caseInfoPopup").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
        $("#caseInfoPopup").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
        $("#caseInfoPopup").dxForm("instance").itemOption("EmptySpace", "visible", false);
        $("#caseInfoPopup").dxForm('instance').repaint();       
        
    }
    this.CancelRequestPayoffPopup = function () {
        $("#RequestPayoffPopup").dxPopup("instance").hide();
        $('#RequestPayoff').dxForm('instance').resetValues();
        $("#RequestPayoff").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
        $("#RequestPayoff").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
        $("#RequestPayoff").dxForm("instance").itemOption("EmptySpace", "visible", false);
        $("#RequestPayoff").dxForm('instance').repaint();       
    }
    this.CreateStatusEntries = function () {
        var form = $("#caseInfoPopup").dxForm().dxForm("instance");
        var PortfolioID = form.option('formData').PortfolioId;
        var CaseId = form.option('formData').CaseId;
        //var PortfolioID = form.getEditor('PortfolioId').option('value');
        if (PortfolioID == null || PortfolioID == "") {
            form.getEditor('CombinedName').focus();
            Common.DisplayError("Please select client name first."); return false;
        }
        let CasePhaseID, CaseStatusReasonID, CasePhaseName, CaseStatusReasonName, Category, CategoryId, LFCasePhaseId, LFCaseStatusDetailId;
        if (form.getEditor('CasePhaseID').option('value') == null) {
            form.getEditor('CasePhaseID').focus();
            Common.DisplayError("Please select case phase."); return false;
        }
        
        var CombinedName = $("#CombinedNameSelectBox").dxSelectBox("instance").option("selectedItem").CombinedName;
        var dataItem = $("#ddlCaseStatusReasonPopup").dxSelectBox("instance").option("selectedItem");
        
        if (dataItem != null) {
            CasePhaseID = dataItem.CasePhaseId;
            CaseStatusReasonID = dataItem.CaseStatusReasonId;
            CasePhaseName = dataItem.CasePhaseName;
            CaseStatusReasonName = dataItem.CaseStatusReasonName;
            Category = dataItem.Category;
            CategoryId = dataItem.CategoryId;
            Status = dataItem.Status;
            StatusId = dataItem.StatusId;
            LFCasePhaseId = dataItem.LfcasePhaseId;
            LFCaseStatusDetailId = dataItem.Id;
        }
        if (LFCaseStatusDetailId == null || LFCaseStatusDetailId == "" || LFCaseStatusDetailId == 0) {
            form.getEditor('CaseStatusReasonID').focus();
            Common.DisplayError("Please select case status."); return false;
        }
        var Note = form.getEditor('Note').option('value');
        var trialDateAssigned = form.getEditor('TrialDateAssigned') != undefined ? new Date(form.getEditor('TrialDateAssigned').option('value')).toLocaleDateString() : null;
        var DepositionsStatusName = form.getEditor('DepositionsStatus') != undefined ? form.getEditor('DepositionsStatus').option('text') : null;
        
        //if (Note == "") {
        //    form.getEditor('Note').focus();
        //    Common.DisplayError("Please enter Notes."); return false;
        //}
        var caseData = {
            PortfolioId: PortfolioID,
            CaseId: CaseId,
            Note: Note,
            CasePhaseID: CasePhaseID,
            CaseStatusReasonID: CaseStatusReasonID,
            CasePhaseName: CasePhaseName,
            CaseStatusReasonName: CaseStatusReasonName,
            Category: Category,
            CategoryId: CategoryId,
            Status: Status,
            StatusId: StatusId,
            LFCasePhaseId: LFCasePhaseId,
            LFCaseStatusDetailId: LFCaseStatusDetailId,
            CombinedName: CombinedName,
            TrialDateAssigned: trialDateAssigned,
            DepositionsStatusName: DepositionsStatusName
        };

        let successFun = function (response) {
            if (response) {
                $("#CombinedNameSelectBox").dxSelectBox('getDataSource').reload();
                //$("#CasePopup").dxPopup("instance").hide();                
                Common.DisplaySuccess("record saved successfully.");
                $('#CaseDueForUpdates').dxDataGrid('instance').refresh();
                sendMessage(CaseId, 'Update for Case:' + CaseId + ' notes: ' + Note, 2000);
                $('#caseInfoPopup').dxForm('instance').resetValues();
                $("#caseInfoPopup").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
                $("#caseInfoPopup").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
                $("#caseInfoPopup").dxForm("instance").itemOption("EmptySpace", "visible", false);
                $("#caseInfoPopup").dxForm('instance').repaint();
                dashboard.FilterDashboard();
                Common.HideLoader();
            }
            else {
                Common.HideLoader();
            }
        }
        Common.Ajax("POST", dashboard.actionUrls.UpdateCaseData, caseData, successFun);
    }
    this.UpdateStatusEntries = function () {
        var grid = $("#CaseDueForUpdates").dxDataGrid("instance");
        let modifiedItems = grid.getController("editing")._editData;
        let Note = $("#txtNote textarea").val();
        let CasePhaseID, CaseStatusReasonID, CasePhaseName, CaseStatusReasonName, Category, CategoryId, LFCasePhaseId, LFCaseStatusDetailId;
        var dataItem = $("#ddlCaseStatusReason").dxSelectBox("instance").option("selectedItem");
        if (dataItem != null) {
            CasePhaseID = dataItem.CasePhaseId;
            CaseStatusReasonID = dataItem.CaseStatusReasonId;
            CasePhaseName = dataItem.CasePhaseName;
            CaseStatusReasonName = dataItem.CaseStatusReasonName;
            Category = dataItem.Category;
            CategoryId = dataItem.CategoryId;
            Status = dataItem.Status;
            StatusId = dataItem.StatusId;
            LFCasePhaseId = dataItem.LfcasePhaseId;
            LFCaseStatusDetailId = dataItem.Id;
        }

        if ($("#ddlCasePhase").dxSelectBox("instance").option("selectedItem") == null) {
            Common.DisplayError("Please select CasePhase."); return false;
        }
        if ($("#ddlCaseStatusReason").dxSelectBox("instance").option("selectedItem") == null) {
            Common.DisplayError("Please select CaseStatus."); return false;
        }
        //if (Note == null || Note == "") {
        //    Common.DisplayError("Please enter Notes."); return false;
        //}
        var trialDateAssigned = $('#TrialDateDB').dxDateBox("instance") != undefined ? new Date($('#TrialDateDB').dxDateBox("instance").option('value')).toLocaleDateString() : null;
        var depositionsStatus = $('#sbDepositionsStatus').dxSelectBox("instance") != undefined ? $('#sbDepositionsStatus').dxSelectBox("instance").option('text') : null;
        var caseData = {
            PortfolioId: modifiedItems[0].oldData.PortfolioId,
            CaseId: modifiedItems[0].oldData.CaseId,
            Note: Note,
            CasePhaseID: CasePhaseID,
            CaseStatusReasonID: CaseStatusReasonID,
            CasePhaseName: CasePhaseName,
            CaseStatusReasonName: CaseStatusReasonName,
            Category: Category,
            CategoryId: CategoryId,
            Status: Status,
            StatusId: StatusId,
            LFCasePhaseId: LFCasePhaseId,
            LFCaseStatusDetailId: LFCaseStatusDetailId,
            CombinedName: modifiedItems[0].oldData.CombinedName,
            TrialDateAssigned: trialDateAssigned,
            DepositionsStatusName: depositionsStatus
        };

        let successFun = function (response) {
            if (response) {
                $("#caseInfoPopupEdit").dxPopup("instance").hide();
                $("#CaseUpdateFormEdit").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
                $("#CaseUpdateFormEdit").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
                $("#CaseUpdateFormEdit").dxForm("instance").itemOption("EmptySpace", "visible", false);
                $("#CaseUpdateFormEdit").dxForm("instance").repaint();
                Common.DisplaySuccess("record saved successfully.")
                $('#CaseDueForUpdates').dxDataGrid('instance').refresh();
                sendMessage(modifiedItems[0].oldData.CaseId, 'Update for Case:' + modifiedItems[0].oldData.CaseId + ' notes: ' + Note, 2000);
                dashboard.FilterDashboard();
                Common.HideLoader();
            }
            else {
                Common.HideLoader();
            }
        }
        Common.Ajax("POST", dashboard.actionUrls.UpdateCaseData, caseData, successFun);
    }
    this.RequestPayoffPopup = function (e) {
        $("#RequestPayoffPopup").dxPopup("option", "title", "Request Payoff for - " + e.row.data.CombinedName);
        let request = {};
        request = { 'PortfolioId': e.row.data.PortfolioId };
        let successFun = function (response) {
            $("#RequestPayoffPopup").dxPopup("instance").show();
            var form = $("#RequestPayoff").dxForm().dxForm("instance");
            if (response != null) {
                var responseDate = new Date(response.CreatedDate);
                form.updateData("Status", "The last payoff request was made on " + responseDate.toLocaleString() + " by " + response.CreatedBy);
            }
            else { form.updateData("Status", "No payoff request made yet"); }
            form.updateData("CombinedName", e.row.data.CombinedName);
            form.updateData("PortfolioId", e.row.data.PortfolioId);
            form.updateData("LawFirmdName", e.row.data.LawFirmdName);
            form.updateData("CaseId", e.row.data.CaseId);
            Common.HideLoader();

        }
        Common.Ajax("GET", dashboard.actionUrls.RequestPayOff, request, successFun);
    }
    this.SubmitPayOffRequest = function () {
        var form = $("#RequestPayoff").dxForm().dxForm("instance");
        var message = form.getEditor('Note').option('value');
        var CombinedName = form.option('formData').CombinedName;
        var LawFirmdName = form.option('formData').LawFirmdName;
        var PortfolioId = form.option('formData').PortfolioId;
        var PayoffRequestReasonID = form.option('formData').PayoffRequestReasonID;        
        var CaseId = form.option('formData').CaseId; 

        if (PayoffRequestReasonID == null) {
            form.getEditor('PayoffRequestReasonID').focus();
            Common.DisplayError("Please select Reason."); return false;
        }

        var PayoffRequestReasonName = $("#ddlPayoffRequestReasonID").dxSelectBox("instance").option("selectedItem").Text;
        var caseData = {
            PortfolioId: PortfolioId,
            Note: message,
            CombinedName: CombinedName,
            PayoffRequestReasonID: PayoffRequestReasonID,
            PayoffRequestReasonName: PayoffRequestReasonName
        };

        let successFun = function (response) {
            if (response) {
                $("#RequestPayoffPopup").dxPopup("instance").hide();
                Common.DisplaySuccess("Payoff Request sent successfully.");
                Common.HideLoader();
                $('#RequestPayoff').dxForm('instance').resetValues();                
                //$("#RequestPayoff").dxForm('instance').repaint();
                $('#CaseDueForUpdates').dxDataGrid('instance').refresh();
                sendMessage(CaseId, 'Payoff Requested By ' + LawFirmdName + ' notes: ' + message, 2000);
            }
            else {
                Common.HideLoader();
            }
        }
        Common.Ajax("GET", dashboard.actionUrls.SendRequestPayOff, caseData, successFun);
    }
    this.showCasePopup = function () {
        var popup = $("#CasePopup").dxPopup("instance");
        popup.show();
    }
    this.OnRowPrepared = function (e) {
        if (e.rowType !== "data")
            return
        if (e.data.NoOfDaysLeft <= -50) {
           // e.rowElement.css("background-color", "#e9ecef");
            e.rowElement.css("font-weight", "bold");
        }
    }
    this.CaseStatusReasonText = function (e) {
        if (e.value == "Select") {
            return e.value = "";
        }
        else
            return e.value;
    }
    this.DOBNULLText = function (e) {
        if (e.valueText == "01/01/1901" || e.valueText == "01/01/1900") {
            return e.valueText = "";
        }
        else
            return e.valueText;
    }
    this.setPopupTitle = function (e) {
        var btns = e.component.content().parent().find(".dx-button.dx-button-has-text");
        for (var i = 0; i < btns.length; i++) {
            var btn = btns[i];
            if (btn.textContent == "Save") {
                $(btn).dxButton("option", "onClick", function () {
                    dashboard.UpdateStatusEntries();
                });
            }
            else {
                $(btn).dxButton("option", "onClick", function () {                   
                    $("#CaseUpdateFormEdit").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
                    $("#CaseUpdateFormEdit").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
                    $("#CaseUpdateFormEdit").dxForm("instance").itemOption("EmptySpace", "visible", false);
                    $("#CaseDueForUpdates").dxDataGrid("cancelEditData");
                });
            }
        }
    }
    this.onEditingStart = function (e) {
        e.component.option("editing.popup.title", "Case Update for - " + e.data.CombinedName);
    }
    this.OnToolbarPreparing = function (e) {
        var dataGrid = e.component;
        e.toolbarOptions.items.unshift(
            {
                location: "after",
                widget: "dxButton",
                locateInMenu: 'auto',
                options: {
                    icon: "refresh",
                    hint: 'Refresh',
                    text: "Refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            }, {
            location: "after",
            widget: "dxButton",
            locateInMenu: 'auto',
            options: {
                icon: "export",
                text: "Export all data",
                hint: "Export all data",
                elementAttr: {
                    "class": "dx-datagrid-export-button"
                },
                onClick: function () {
                    e.component.exportToExcel(false);
                }
            }
        });
    }
    this.ApprovedAmount = function (cellElement, cellInfo) {
        var approvedAmt = DevExpress.localization.formatNumber(cellInfo.data.ApprovalAmount, { type: "currency", currency: "USD", precision: 2 })
        $('<a/>')
            .attr({ href: 'javascript:dashboard.ShowAmountFundedPopup(' + cellInfo.data.CaseId + ',' + cellInfo.data.PortfolioId + ',\'' + cellInfo.data.CombinedName + '\')' })
            .text(approvedAmt)
            .appendTo(cellElement);        
    }
    this.ShowAmountFundedPopup = function (CaseId, PortfolioId, ClientName) {

        buttonItems = [
            {
                toolbar: 'bottom', location: 'after', widget: 'button',
                options: {
                    text: 'OK',
                    onClick: function () {
                        var e = $("#popupContainer").dxPopup("instance")
                        e.element().attr('act', 1);
                        e.hide();
                    }
                }
            },
            {
                toolbar: 'bottom', location: 'after', widget: 'button', options: {
                    text: 'Cancel', onClick: function () {
                        var e = $("#popupContainer").dxPopup("instance");
                        e.element().attr('act', 0);
                        e.hide();
                    }
                }
            }
        ];

        var defer = $.Deferred();
        if ($('#popupContainer').length <= 0) {
            $("html").prepend('<div id="popupContainer"></div>');
        }
        $('#popupContainer').dxPopup({
            contentTemplate: $('#content_template'),
            showTitle: true,
            title: 'Details for Client: ' + ClientName,
            buttons: buttonItems,
            showCloseButton: true,
            fullScreen: false,
            width: function () {
                if (window.innerWidth < 768)
                    return window.innerWidth / 1.2
                if (window.innerWidth < 992) {
                    return window.innerWidth / 1.2
                }
                if (window.innerWidth < 1200) {
                    return window.innerWidth / 1.5
                }
                else
                    return window.innerWidth / 2.3
            },
            height: function () {
                return window.innerHeight / 1.5;
            },
            dragEnabled: false,
            closeOnOutsideClick: false,
            onShown: function (e) {
                e.element.attr('act', 0);
                let request = {};
                request = { 'CaseId': CaseId };
                let successFun = function (response) {
                    $("#AmountFundedGrid").dxDataGrid({
                        dataSource: response,
                        elementAttr: {
                            class: "table table-hover"
                        },
                        columns: [
                            {
                                caption: "Date Funded",
                                dataField: 'CheckDate',
                                dataType: 'date'
                            },
                            {
                                caption: "Payee",
                                dataField: 'ClientTypeName'
                            },                            
                            {
                                caption: "Funded Amount",
                                dataField: 'Amount',
                                dataType: "number",
                                format: { type: "currency", precision: 2 }
                            },
                            {
                                caption: "Disbursement Type",
                                dataField: 'DisbursementType'
                            },                            
                        ],
                        summary: {
                            totalItems: [{
                                column: "Amount",
                                summaryType: "sum",
                                valueFormat: { type: "currency", precision: 2 }
                            }]
                        },
                        sorting: {
                            mode: "multiple"
                        },
                        scrolling: {
                            mode: 'infinite'
                        },
                        showColumnLines: true,
                        showRowLines: true,
                        rowAlternationEnabled: true,
                        showBorders: true,
                        columnAutoWidth: true,
                        groupPanel: {
                            visible: true
                        },
                        selection: {
                            mode: "single"
                        },
                        onCellPrepared: function (e) {
                            if (e.rowType == "header") {
                                e.cellElement.css("text-align", "center");
                            }
                            if (e.rowType == "data")
                                e.cellElement.css("text-align", "center");
                        }
                    });
                    Common.HideLoader();

                }
                Common.Ajax("GET", dashboard.actionUrls.FundedAmount, request, successFun);
            },
            onHidden: function (e) {
                if (e.element.attr('act') == "1") {
                    defer.resolve(true, $("#AmountFundedGrid").dxDataGrid("instance").getSelectedRowsData());
                }
                else {
                    defer.resolve(false, []);
                }
                $("#AmountFundedGrid").dxDataGrid("instance").option("dataSource", null);
            },
            animation: {
                show: { type: "slide", from: { opacity: 1, top: -$(window).height() }, to: { top: 50 } },
                hide: { type: "slide", from: { top: 50 }, to: { top: -$(window).height() } }
            }
        });

        $("#popupContainer").dxPopup("instance").show();
        return defer.promise();
    }
    this.OnCasePhaseChange = function (e) {
        $("#ddlCaseStatusReason").dxSelectBox('instance').option('value', 0);
        $("#ddlCaseStatusReason").dxSelectBox("instance").getDataSource().filter(["LfcasePhaseId", "=", e.value]);
        $("#ddlCaseStatusReason").dxSelectBox("instance").getDataSource().reload();
        $("#ddlCaseStatusReason").dxSelectBox("instance").option('disabled', false);
    }
    this.CaseStatusContentReady = function (e) {
        let items = e.component.getDataSource().items();
        if (items.length == 1) {
            if (items[0].LfcaseDetailsName == 'Select') {
                $("#ddlCaseStatusReason").dxSelectBox("instance").option('disabled', true);
                e.component.option('value', items.length > 0 ? items[0].Id : null);
            }
        }
        else
            $("#ddlCaseStatusReason").dxSelectBox("instance").option('disabled', false);
    }    
    this.OnCasePhaseChangePopup = function (e) {
        $("#ddlCaseStatusReasonPopup").dxSelectBox('instance').option('value', 0);
        $("#ddlCaseStatusReasonPopup").dxSelectBox("instance").getDataSource().filter(["LfcasePhaseId", "=", e.value]);
        $("#ddlCaseStatusReasonPopup").dxSelectBox("instance").getDataSource().reload();
        $("#ddlCaseStatusReasonPopup").dxSelectBox("instance").option('disabled', false);
    }
    this.OnCaseStatusClick = function (e) {
        $("#CaseUpdateFormEdit").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
        $("#CaseUpdateFormEdit").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
        $("#CaseUpdateFormEdit").dxForm("instance").itemOption("EmptySpace", "visible", false);
        if (e.itemData != null && e.itemData.LfcaseDetailsName.toLowerCase().trim() == 'trial date assigned') {
            $("#CaseUpdateFormEdit").dxForm("instance").itemOption("TrialDateAssigned", "visible", true);
            $("#CaseUpdateFormEdit").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
            $("#CaseUpdateFormEdit").dxForm("instance").itemOption("EmptySpace", "visible", true);
            $("#CaseUpdateFormEdit").dxForm('instance').repaint();  
        }
        if (e.itemData != null && e.itemData.LfcaseDetailsName.toLowerCase().trim() == 'depositions') {
            $("#CaseUpdateFormEdit").dxForm("instance").itemOption("DepositionsStatus", "visible", true);
            $("#CaseUpdateFormEdit").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
            $("#CaseUpdateFormEdit").dxForm("instance").itemOption("EmptySpace", "visible", true);
            $("#CaseUpdateFormEdit").dxForm('instance').repaint();  
        }
        $("#ddlCaseStatusReason").dxSelectBox("instance").getDataSource().filter(["LfcasePhaseId", "=", e.itemData.LfcasePhaseId]);            
        $("#ddlCaseStatusReason").dxSelectBox("instance").option('disabled', false);
    }
    //this.OnCaseStatusClickPayOff = function (e) {
    //    $("#RequestPayoff").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
    //    $("#RequestPayoff").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
    //    $("#RequestPayoff").dxForm("instance").itemOption("EmptySpace", "visible", false);
    //    if (e.itemData != null && e.itemData.LfcaseDetailsName.toLowerCase().trim() == 'trial date assigned') {
    //        $("#RequestPayoff").dxForm("instance").itemOption("TrialDateAssigned", "visible", true);
    //        $("#RequestPayoff").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
    //        $("#RequestPayoff").dxForm("instance").itemOption("EmptySpace", "visible", true);
    //        $("#RequestPayoff").dxForm('instance').repaint();
    //    }
    //    if (e.itemData != null && e.itemData.LfcaseDetailsName.toLowerCase().trim() == 'depositions') {
    //        $("#RequestPayoff").dxForm("instance").itemOption("DepositionsStatus", "visible", true);
    //        $("#RequestPayoff").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
    //        $("#RequestPayoff").dxForm("instance").itemOption("EmptySpace", "visible", true);
    //        $("#RequestPayoff").dxForm('instance').repaint();
    //    }
        
    //    $("#ddlCaseStatusReasonPayOff").dxSelectBox("instance").getDataSource().filter(["LfcasePhaseId", "=", e.itemData.LfcasePhaseId]);
    //    $("#ddlCaseStatusReasonPayOff").dxSelectBox("instance").option('disabled', false);
    //}
    this.CaseStatusContentReadyPopup = function (e) {
        let items = e.component.getDataSource().items();
        if (items.length == 1) {
            if (items[0].LfcaseDetailsName == 'Select') {
                $("#caseInfoPopup").dxForm('instance').getEditor("CaseStatusReasonID").option("disabled", true);
                e.component.option('value', items.length > 0 ? items[0].Id : null);
            }
        }
        else {
            $("#caseInfoPopup").dxForm('instance').getEditor("CaseStatusReasonID").option("disabled", false);  
        }
        
    }
    this.OnCaseStatusClickPopup = function (e) {
        $("#caseInfoPopup").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
        $("#caseInfoPopup").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
        $("#caseInfoPopup").dxForm("instance").itemOption("EmptySpace", "visible", false);
        if (e.itemData != null && e.itemData.LfcaseDetailsName.toLowerCase().trim() == 'trial date assigned') {
            $("#caseInfoPopup").dxForm("instance").itemOption("TrialDateAssigned", "visible", true);
            $("#caseInfoPopup").dxForm("instance").itemOption("DepositionsStatus", "visible", false);
            $("#caseInfoPopup").dxForm("instance").itemOption("EmptySpace", "visible", true);
            $("#caseInfoPopup").dxForm('instance').repaint();
        }
        if (e.itemData != null && e.itemData.LfcaseDetailsName.toLowerCase().trim() == 'depositions') {
            $("#caseInfoPopup").dxForm("instance").itemOption("DepositionsStatus", "visible", true);
            $("#caseInfoPopup").dxForm("instance").itemOption("TrialDateAssigned", "visible", false);
            $("#caseInfoPopup").dxForm("instance").itemOption("EmptySpace", "visible", true);
            $("#caseInfoPopup").dxForm('instance').repaint();
        }       
        $("#ddlCaseStatusReasonPopup").dxSelectBox("instance").getDataSource().filter(["LfcasePhaseId", "=", e.itemData.LfcasePhaseId]);
        $("#caseInfoPopup").dxForm('instance').getEditor("CaseStatusReasonID").option("disabled", false);
    }
    //this.OnCasePhaseChangePayOff = function (e) {
    //    $("#ddlCaseStatusReasonPayOff").dxSelectBox('instance').option('value', 0);
    //    $("#ddlCaseStatusReasonPayOff").dxSelectBox("instance").getDataSource().filter(["LfcasePhaseId", "=", e.value]);
    //    $("#ddlCaseStatusReasonPayOff").dxSelectBox("instance").getDataSource().reload();
    //    $("#ddlCaseStatusReasonPayOff").dxSelectBox("instance").option('disabled', false);
    //}
    //this.CaseStatusContentReadyPayOff = function (e) {
    //    let items = e.component.getDataSource().items();
    //    if (items.length == 1) {
    //        if (items[0].LfcaseDetailsName == 'Select') {
    //            $("#ddlCaseStatusReasonPayOff").dxSelectBox("instance").option('disabled', true);
    //        }
    //    }
    //    else
    //        $("#ddlCaseStatusReasonPayOff").dxSelectBox("instance").option('disabled', false);
    //    e.component.option('value', items.length > 0 ? items[0].Id : null);
    //}
    this.CaseInfoScreenByWidth = function (width) {
        if (width < 768) {
            $("#CasePopup").dxPopup("option", "width", "80%");
            return "xs";
        }
        else if (width < 992) {
            $("#CasePopup").dxPopup("option", "width", "75%");
            return "sm";
        }
        else if (width < 1200) {
            $("#CasePopup").dxPopup("option", "width", "55%");
            //popup.option("max-width", "55%");
            return "md";
        }
        else {
            $("#CasePopup").dxPopup("option", "width", "50%");
            $("#CasePopup").dxPopup("option", "height", "75%");
            //popup.option("max-width", "50%");
            return "lg";
        }
        //if (width < 640)
        //    return 'xs';
        //if (width < 1280)
        //    return 'sm';
        //if (width < 1920)
        //    return 'md';
        //return 'lg';
    }
    this.CaseInfoEditScreenByWidth = function (width) {
        if (width < 768) {
            $("#caseInfoPopupEdit").dxPopup("option", "width", "80%");
            return "xs";
        }
        if (width < 992) {
            $("#caseInfoPopupEdit").dxPopup("option", "width", "75%");
            return "sm";
        }
        if (width < 1200) {
            $("#caseInfoPopupEdit").dxPopup("option", "width", "55%");
            return "md";
        }
        else {
            $("#caseInfoPopupEdit").dxPopup("option", "width", "50%");
            return "lg";
        }
    }
    this.RequestPayoffScreenByWidth = function (width) {
        if (width < 768) {
            $("#RequestPayoffPopup").dxPopup("option", "width", "80%");
            return "xs";
        }
        if (width < 992) {
            $("#RequestPayoffPopup").dxPopup("option", "width", "75%");
            return "sm";
        }
        if (width < 1200) {
            $("#RequestPayoffPopup").dxPopup("option", "width", "55%");
            return "md";
        }
        else {
            $("#RequestPayoffPopup").dxPopup("option", "width", "50%");
            $("#RequestPayoffPopup").dxPopup("option", "height", "60%");
            return "lg";
        }
    }
    this.RequestPayoffOnContentReady = function (e) {
        var width = $(window).width();
        if (width < 768)
            e.component.option('labelLocation', 'top')
        if (width < 992) {
            e.component.option('labelLocation', 'top')
        }
        if (width < 1200) {
            e.component.option('labelLocation', 'top')
        }
        else
            e.component.option('labelLocation', 'left')
    }
};