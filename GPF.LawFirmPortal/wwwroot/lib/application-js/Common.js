﻿$.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    return this;
}
var Common = {
    Ajax: function (httpMethod, url, data, successCallBack, async, cache) {
        if (typeof async == "undefined") {
            async = true;
        }
        if (typeof cache == "undefined") {
            cache = false;
        }
        var ajaxObj = $.ajax({
            beforeSend: function () {
                if (url.indexOf('AutoSave') == -1) {
                    Common.ShowLoader();
                }
            },
            type: httpMethod.toUpperCase(),
            url: url,
            data: data,
            datatype: "json",
            async: async,
            cache: cache,
            success: successCallBack,
            error: function (err, type, httpStatus) {
                Common.AjaxFailureCallback(err, type, httpStatus);
            },
            complete: function () {
                if (url.indexOf('AutoSave') == -1) {
                    Common.HideLoader();
                }
            }
        });

        return ajaxObj;
    },

    AjaxFailureCallback: function (err, type, httpStatus) {
        if (err.status == 401) {
            window.location.href = '/Account/Login';
        }
        else {
            var failureMessage = 'Error occurred in ajax call' + err.status + " - " + err.responseText;
            console.log(failureMessage);
            this.DisplayError('An error occurred while processing your request, Please Try Again Later!');
        }
    },

    DisplaySuccess: function (message) {
        DevExpress.ui.notify({
            message: message,
            type: "success",
            displayTime: 2000,
            width: 400,
            position: { my: "right", at: 'center', of: "#toastMessage", boundaryOffset: "50 -50" }
        });
    },
    DisplayError: function (message) {
        //this.HideAutoSaveFailureLoader();
        DevExpress.ui.notify({
            message: message, type: "error", displayTime: 2000, width: 400, position: { my: "right", at: 'center', of: "#toastMessage",boundaryOffset: "50 -50" },
        });
    },
    DisplayInfo: function (message) {
        DevExpress.ui.notify({ message: message, type: "info", displayTime: 2000, width: 400, position: { my: "right", at: 'center', of: "#toastMessage", boundaryOffset: "50 -50" } });
    },       
    Confirm: function (message, successFun, cancelFun) {
        DevExpress.ui.dialog.confirm(message, "Confirmation Dialog")
            .done(function (result) {
                result ? successFun() : cancelFun();
            });
    },    
    ShowLoader: function () {
        $.blockUI({
            css: {
                backgroundColor: 'transparent',
                baseZ: 99999,
                border: 'none'
            },
            message: '<div class="spinner"></div><h3 class="spinnerText">Loading...</h3>',
            baseZ: 99999,
            overlayCSS: {
                backgroundColor: '#FFFFFF',
                opacity: 0.6,
                baseZ:99999,
                cursor: 'wait'
            }
        });
        $('.blockUI.blockMsg').center();
    },
    HideLoader: function () {
        $.unblockUI();
    }
}

