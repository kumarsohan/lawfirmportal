﻿"use strict";
var groupName = "LawFirmPortal";
var connection = new signalR.HubConnectionBuilder().withUrl("/NotificationHub").build();

connection.start().catch(function (err) {
    return console.error(err.toString());
}).then(function () {
    connection.invoke('GetConnectionId').then(function () {
    })
});

connection.on("ReceiveNotification", function (content) {
    var data = content;
    $('div.notify').show();
    $('#messagenotification').append($('<a href="javascript:void(0)" class="message-item d-flex align-items-center border-bottom px-3 py-2"><span class= "btn btn-danger rounded-circle btn-circle" > <i class="fa fa-link"></i></span><div class="w-75 d-inline-block v-middle pl-2"><span class="font-12 text-nowrap d-block text-muted text-truncate">' + data.Note + '</span></div></a >'));
});


$(function () {
    // Click on notification icon for show notification
    $('#notification').click(function (e) {       
        $('div.notify').hide();
        updateNotification();
    });
    // hide notifications
    $('html').click(function () {
        $('.noti-content').hide();
    });

    
})
function MarkRead(id) {
    $.ajax({
        type: 'POST',
        data: { Ids: id },
        url: '/home/UpdateNotifications',
        success: function (response) {
            updateNotification();
        },
        error: function (error) {
            console.log(error);
        }
    });
}
function MarkAllRead() {
    var notificationId = '';
    $('#messagenotification h5').attr('id');
    $('#messagenotification h5').each(function () {
        notificationId += $(this).attr('id') + ',';
    });
    $.ajax({
        type: 'POST',
        data: { Ids: notificationId.replace(/,\s*$/, "") },
        url: '/home/UpdateNotifications',
        success: function (response) {
            updateNotification();
        },
        error: function (error) {
            console.log(error);
        }
    });
}
// update notification
function updateNotification() {
    $('#messagenotification').empty();
    $('#messagenotification').append($('<li>Loading...</li>'));
    $.ajax({
        type: 'GET',
        url: '/home/GetNotifications',
        success: function (response) {
            $('#messagenotification').empty();
            if (response.length == 0) {
                $('#messagenotification').append($('<a href="javascript:void(0)" class="message-item d-flex align-items-center border-bottom px-3 py-2"><span class= "btn btn-danger rounded-circle btn-circle" > <i class="fa fa-link"></i></span><div class="w-75 d-inline-block v-middle pl-2"><span class="font-12 text-nowrap d-block text-muted text-truncate">Currently You Have No New Notifications</span></div></a >'));
            }
            $.each(response, function (index, value) {
                $('#messagenotification').append($('<a href="javascript:void(0)" onclick="MarkRead(' + value.NotificationId + ');" class="message-item d-flex align-items-center border-bottom px-3 py-2"><span class= "btn btn-danger rounded-circle btn-circle" > <i class="fa fa-link"></i></span><div class="w-75 d-inline-block v-middle pl-2"><h5 id=' + value.NotificationId + ' class="message-title mb-0 mt-1">' + value.DisplayName + '</h5><span class="font-12 text-nowrap d-block text-muted text-truncate">' + value.Message + '</span></div></a >'));
            });
        },
        error: function (error) {
            console.log(error);
        }
    })
}    