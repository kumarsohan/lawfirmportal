﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using GPF.LawFirmPortal.Application.Admin.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GPF.LawFirmPortal.Controllers
{
    public class UserActivityController : BaseController
    {
        private readonly IMediator _mediator;
        public UserActivityController(IMediator mediator)
        {
            this._mediator = mediator;
        }
        public IActionResult Index()
        {
            ViewBag.Title = "User Activity's";
            return View();
        }
        [HttpGet]
        public async Task<object> GetUserActivity(DataSourceLoadOptions loadOptions)
        {           
            var userActivities = await Mediator.Send(new GetUserActivityQuery()).ConfigureAwait(true);
            return DataSourceLoader.Load(userActivities, loadOptions);
        }
    }
}