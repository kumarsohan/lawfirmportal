﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Audit.Mvc;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.Common.Utility;
using GPF.LawFirmPortal.Models;
using GPF.LawFirmPortal.Web.Common;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using static GPF.LawFirmPortal.Application.Common.Utility.AppConstants;

namespace GPF.LawFirmPortal.Controllers
{
    public class AccountController : BaseController
    {
        private readonly ILdapService _authService;
        private readonly ICommonService _commonService;
        private readonly IConfiguration _configuration;
        public AccountController(IConfiguration configuration, ILdapService authService,ICommonService commonService)
        {
            _authService = authService;
            _configuration = configuration;
            _commonService = commonService;
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [Audit(EventTypeName = "Login", IncludeHeaders = false, SerializeActionParameters = true, IncludeRequestBody = false, IncludeResponseBody = true)]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = _authService.ValidateADDirectory(model.UserName, model.Password);                    
                    // If the user is authenticated, store its claims to cookie
                    if (user != null)
                    {
                        string lawFirmName = string.Empty;//, shortName = string.Empty;
                        if (user.LawFirmID != "")                        
                            lawFirmName = _commonService.GetLawFirmName(Convert.ToInt32(user.LawFirmID));                        
                        else
                            lawFirmName = AppConstants.GPFSettings.NOLOGO;

                        //shortName = string.Empty;
                        //lawFirmName.Split(' ').Take(3).ToList().ForEach(i => shortName += i[0].ToString());
                        var userClaims = new List<Claim>
                        {
                            new Claim("UserName", user.Username),
                            new Claim("Email", user.Email),
                            new Claim("DisplayName",user.DisplayName),
                            new Claim("LawFirmId",user.LawFirmID),
                            new Claim("UserGroup",user.GroupName),
                            new Claim("LawFirmName",lawFirmName),
                            //new Claim("LawFirmSName",shortName)
                        };
                        //we can add custom claims based on the AD user's groups
                        var claimsIdentity = new ClaimsIdentity(userClaims, _authService.GetType().Name);
                        claimsIdentity.AddClaim(new Claim("lawfirmportal.user.auth", "true"));
                        await HttpContext.SignInAsync(
                          CookieAuthenticationDefaults.AuthenticationScheme,
                            new ClaimsPrincipal(claimsIdentity),
                            new AuthenticationProperties
                            {
                                IsPersistent = model.RememberMe
                                //i = TimeSpan.FromMinutes(Convert.ToInt32(_configuration[GPFSettings.SESSIONTIMEOUT], CultureInfo.InvariantCulture))
                            }
                        );
                        return RedirectToAction("Dashboard", "Home");
                    }
                    ModelState.AddModelError("", @"Your username or password is incorrect. Please try again.");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View(model);
        }
        [Audit(EventTypeName = "Logout", IncludeHeaders = false, SerializeActionParameters = true, IncludeRequestBody = false, IncludeResponseBody = true)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            TempData.Clear();
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction(nameof(HomeController.Dashboard), "Home");
        }
    }
}