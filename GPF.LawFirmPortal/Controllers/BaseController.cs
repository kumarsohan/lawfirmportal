﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GPF.LawFirmPortal.Attributes;
using GPF.LawFirmPortal.Web.Common;
using MediatR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using static GPF.LawFirmPortal.Application.Common.Utility.AppConstants;

namespace GPF.LawFirmPortal.Controllers
{
    [Authorize(Policy = "Require.LawFirmPortal.User.Auth", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    [BreadcrumbActionFilter]    
    public class BaseController : Controller
    {
        private IMediator _mediator;
        private bool _isUserAdmin { get; set; }
        private readonly IHttpContextAccessor _contextAccessor;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
        public BaseController()
        {
            _contextAccessor = new HttpContextAccessor();
        }

        public bool IsUserAdmin
        {
            get
            {
                if (_isUserAdmin == false)
                {
                    var Group = ClaimsExtensions.GetClaimValue(HttpContext.User, "UserGroup");
                    var configuration = this.HttpContext.RequestServices.GetService<IConfiguration>();
                    var adminRole = configuration[LDAPSettings.ADMINROLE];
                    if (adminRole == Group)
                    {
                        //HttpContext.Session.SetString("IsUserAdmin", "true");
                        _isUserAdmin = true;
                    }
                    return _isUserAdmin;
                }

                return _isUserAdmin;
            }

            set
            {
                _isUserAdmin = value;
            }
        }
    }
}