﻿using Audit.Mvc;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using GPF.LawFirmPortal.Application.CaseUpdate.Command;
using GPF.LawFirmPortal.Application.CaseUpdate.Queries;
using GPF.LawFirmPortal.Application.CaseUpdate.ViewModel;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Application.LawFirmDashboard.Queries;
using GPF.LawFirmPortal.Application.LawFirmDashboard.ViewModel;
using GPF.LawFirmPortal.Application.Notifications.Command;
using GPF.LawFirmPortal.Application.Notifications.Models;
using GPF.LawFirmPortal.Application.Notifications.Queries;
using GPF.LawFirmPortal.Hubs;
using GPF.LawFirmPortal.Models;
using GPF.LawFirmPortal.Web.Common;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using static GPF.LawFirmPortal.Application.Common.Utility.AppConstants;

namespace GPF.LawFirmPortal.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        private List<int> LawFirmId;
        private LawFirmDashboardModel _firmDashboardModel;
        private readonly IEmailSender _emailSender;
        private readonly IUserConnectionManager _userConnectionManager;
        private readonly IHubContext<NotificationHub> _notificationHubContext;
        public HomeController(ILogger<HomeController> logger, IMediator mediator, IConfiguration configuration, IEmailSender emailSender, IHubContext<NotificationHub> notificationHubContext,
            IUserConnectionManager userConnectionManager)
        {
            _logger = logger;
            _emailSender = emailSender;
            _configuration = configuration;
            this._mediator = mediator;
            this._firmDashboardModel = new LawFirmDashboardModel();
            this.LawFirmId = new List<int>();
            _notificationHubContext = notificationHubContext;
            _userConnectionManager = userConnectionManager;
        }
        [Audit(EventTypeName = "Dashboard", IncludeHeaders = false, SerializeActionParameters = true, IncludeRequestBody = false, IncludeResponseBody = true)]
        public async Task<IActionResult> Dashboard()
        {
            ViewBag.Title = "Dashboard";
            ViewBag.IsUserAdmin = IsUserAdmin;
            IEnumerable<int> tempLawFirms = TempData["LawFirmFilter"] as IEnumerable<int>;
            //TempData.Keep("LawFirmFilter");
            _firmDashboardModel.FirmValues = tempLawFirms != null ? string.Join(",", tempLawFirms.Select(n => n).ToArray()) : "";
            return View(_firmDashboardModel);
        }
        public async Task<IActionResult> LoadDashBoardData(DateTime StartDate, DateTime EndDate,List<int> LawFirms)
        {
            if (IsUserAdmin)
            {
                LawFirmId = LawFirms;
                TempData["LawFirmFilter"] = LawFirmId;
            }
            else
            {
                var lawFirmId = ClaimsExtensions.GetClaimValue(HttpContext.User, "LawFirmId");
                if (lawFirmId == "")
                    LawFirmId.Add(0);
                else
                    LawFirmId.Add(Convert.ToInt32(lawFirmId));
            }
            var dashboard = await Mediator.Send(new GetLawFirmDataQuery()
            {
                LawFirmId = LawFirmId
            }).ConfigureAwait(true);
            return Json(dashboard);
        }
        [HttpGet]
        public async Task<object> GetLawFirms(DataSourceLoadOptions loadOptions)
        {
            var lawFirms = await Mediator.Send(new GetAllLawFirmsQuery() { }).ConfigureAwait(true);
            return DataSourceLoader.Load(lawFirms, loadOptions);
        }       

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public async Task<IActionResult> GetNotifications()
        {
            var LawFirmId = ClaimsExtensions.GetClaimValue(HttpContext.User, "LawFirmId");
            var notifications = await Mediator.Send(new GetNotificationsQuery()
            {
                LawFirmID = IsUserAdmin ? "0" : LawFirmId,
                IsUserAdmin = IsUserAdmin
            }).ConfigureAwait(true);
            return new JsonResult(notifications);
        }
        public async Task<IActionResult> UpdateNotifications(string Ids)
        {
            var LawFirmId = ClaimsExtensions.GetClaimValue(HttpContext.User, "LawFirmId");
            var notifications = await Mediator.Send(new UpdateNotificationCommand()
            {
                NotificationId = Ids
            }).ConfigureAwait(true);
            return new JsonResult(notifications);
        }
        [HttpGet]
        public async Task<object> GetClientNames(DataSourceLoadOptions loadOptions)
        {
            if (IsUserAdmin)
            {
                var lawFirmIds = TempData["LawFirmFilter"] as IEnumerable<int> ?? new List<int>();
                foreach (int id in lawFirmIds)
                {
                    LawFirmId.Add(id);
                }
                TempData.Keep("LawFirmFilter");
            }
            else
                LawFirmId.Add(Convert.ToInt32(ClaimsExtensions.GetClaimValue(HttpContext.User, "LawFirmId")));

            var caseDueUpdates = await Mediator.Send(new GetClientNamesQuery()
            {
                searchType = GetClientNamesQuery.SearchType.All,
                LawFirmId = LawFirmId
            }
            ).ConfigureAwait(true);
            return DataSourceLoader.Load(caseDueUpdates, loadOptions);
        }
        public async Task<IActionResult> LoadClientInfo(int PortfolioId)
        {
            var caseDueUpdates = await Mediator.Send(new GetClientNamesQuery()
            {
                searchType = GetClientNamesQuery.SearchType.Specific,
                PortfolioId = PortfolioId
            }
            ).ConfigureAwait(true);
            return Json(caseDueUpdates);
        }
        #region AllCaseList
        [HttpGet]
        public async Task<object> GetCaseDueUpdate(DataSourceLoadOptions loadOptions)
        {
            if (IsUserAdmin)
            {
                var lawFirmIds = TempData["LawFirmFilter"] as IEnumerable<int> ?? new List<int>();
                foreach (int id in lawFirmIds)
                {
                    LawFirmId.Add(id);
                }
                TempData.Keep("LawFirmFilter");
            }
            else
            {
                var lawFirmId = ClaimsExtensions.GetClaimValue(HttpContext.User, "LawFirmId");
                if (lawFirmId == "")
                    LawFirmId.Add(0);
                else LawFirmId.Add(Convert.ToInt32(lawFirmId));
            }
               
            var caseDueUpdates = await Mediator.Send(new GetCaseDueforUpdatesQuery()
            {
                LawFirmId = LawFirmId
            }
            ).ConfigureAwait(true);
            return DataSourceLoader.Load(caseDueUpdates, loadOptions);
        }
        [HttpGet]
        public async Task<object> GetCasePhase(DataSourceLoadOptions loadOptions)
        {
            var caseDueUpdates = await Mediator.Send(new GetCasePhaseQuery() { }).ConfigureAwait(true);
            return DataSourceLoader.Load(caseDueUpdates, loadOptions);
        }
        [HttpGet]
        public async Task<object> GetReasons(DataSourceLoadOptions loadOptions)
        {
            var caseDueUpdates = await Mediator.Send(new GetReasonQuery() { }).ConfigureAwait(true);
            return DataSourceLoader.Load(caseDueUpdates, loadOptions);
        }
        [HttpGet]
        public async Task<object> GetRequestPayOff(int PortfolioId)
        {
            var caseDueUpdates = await Mediator.Send(new GetRequestPayoffQuery() { PortfolioId = PortfolioId }).ConfigureAwait(true);
            return Json(caseDueUpdates);
        }
        
        #endregion
        [HttpPost]
        public async Task<IActionResult> UpdateCaseData(CaseDueUpdatesViewModel caseDueUpdates)
        {
            var comments = caseDueUpdates.Note;
            if (caseDueUpdates.TrialDateAssigned != null)
            {
                caseDueUpdates.Note = caseDueUpdates.Note+ " TrialDate Assigned:-" + caseDueUpdates.TrialDateAssigned;
            }
            else if (caseDueUpdates.DepositionsStatusName != null)
            {
                caseDueUpdates.Note = caseDueUpdates.Note + " Depositions Status:-" + caseDueUpdates.DepositionsStatusName;
            }
            var sourceId = await Mediator.Send(new UpdateCaseStatusCommand
            {
                caseDueUpdatesViewModel = caseDueUpdates,
                isRequestPayOff = false
            }
            ).ConfigureAwait(true);

            caseDueUpdates.Note = comments;
            string LawFirmId = ClaimsExtensions.GetClaimValue(HttpContext.User, "LawFirmId");
            await Mediator.Send(new AddNotificationCommand
            {
                Message = caseDueUpdates.Note,
                NotificationState = false,
                DisplayName = ClaimsExtensions.GetClaimValue(HttpContext.User, "DisplayName"),
                LawFirmID = LawFirmId,
                UserGroupName = ClaimsExtensions.GetClaimValue(HttpContext.User, "UserGroup"),
                UserName = ClaimsExtensions.GetClaimValue(HttpContext.User, "UserName")
            }).ConfigureAwait(true);

            await Mediator.Send(new CaseDueUpdateCommand 
            {
                caseUpdatesAuditViewModel=new CaseUpdatesViewModel()
                {
                    CaseId = caseDueUpdates.CaseId,
                    Comment = caseDueUpdates.Note,
                    CasePhaseId = caseDueUpdates.LFCasePhaseId,
                    CaseStatusDetailId = caseDueUpdates.LFCaseStatusDetailId,
                    CreatedBy = ClaimsExtensions.GetClaimValue(HttpContext.User, "DisplayName"),
                    IsAttorneyComment = IsUserAdmin == true ? false : true,
                    PortfolioId = caseDueUpdates.PortfolioId
                }
            }).ConfigureAwait(true);

            var emailMapping=await Mediator.Send(new GetEmailTriggerMappingQuery() { CaseStatusDetailId = caseDueUpdates.LFCaseStatusDetailId }).ConfigureAwait(true);
            if (emailMapping==null)
            {
                return Ok(sourceId);
            }
            Dictionary<string, string> placeHolders = new Dictionary<string, string>
                {
                    { "ClientName", caseDueUpdates.CombinedName},
                    { "CasePhase", caseDueUpdates.LFCaseDetailsName },
                    { "CaseDetails", caseDueUpdates.CaseStatusReasonName},
                    { "Message", caseDueUpdates.Note},
                    { "Department", emailMapping.DepartmentName}
            };

            await _emailSender.SendEmailAsync(
            new MessageDto
            {
                TemplateId = (int)EmailTemplates.CaseUpdate,
                To = emailMapping.EmailMember
            }, null, placeHolders).ConfigureAwait(true);
            var connections = _userConnectionManager.GetUserConnections(ClaimsExtensions.GetClaimValue(HttpContext.User, "UserName"));
            await _notificationHubContext.Clients.GroupExcept("NotificationGroup", connections).SendAsync("ReceiveNotification", caseDueUpdates);
            return Ok(sourceId);
        }
        public async Task<IActionResult> SendRequestPayOff(RequestPayoffDataViewModel caseData)
        {
            var Message = caseData.Note;

            var UserName = ClaimsExtensions.GetClaimValue(HttpContext.User, "DisplayName");
            var payOffData = await Mediator.Send(new GetPayOffDataQuery() { PortFolioId = caseData.PortfolioId }).ConfigureAwait(true);
            Dictionary<string, string> placeHolders = new Dictionary<string, string>
                {
                    { "ClientName", payOffData.ClientName},
                    { "MostRecentSP", payOffData.MostRecentSP },
                    { "CurrentLawFirm", payOffData.LawFirm},
                    { "InsuranceInfo", payOffData.InsuranceCompany },
                    { "Emailed", payOffData.Emailed},
                    { "IndexNo", payOffData.IndexNumber },
                    { "Faxed", payOffData.Faxed},
                    { "countyItem", payOffData.County },
                    { "StateCurrentLF", payOffData.CaseStateCurrentLF},
                    { "TotalFunding", string.Format("{0:C}", payOffData.AmountOwed)},
                    { "PriorityName", payOffData.POPriority},
                    { "CV_ClientLevel", string.Format("{0:C}",payOffData.CV_Clientlevel) },
                    { "TypeName", payOffData.POType},
                    { "CategoryName", payOffData.Category },
                    { "RequestedByName", payOffData.PORequestedBy},
                    { "StatusName", payOffData.CurrentStatus },
                    { "RequestersName", payOffData.PORequestersName},
                    { "CaseStatusReasonName", caseData.PayoffRequestReasonName },
                    { "ContactTypeName", payOffData.POContractType},
                    { "FULLNAME", UserName },
                    { "CaseDaysOut", payOffData.PODaysOut.ToString()},
                    { "CommentsNotes", Message},
                    { "ContactPerson",payOffData.eCourts==true && payOffData.ContactPerson==null?"eCourts": payOffData.ContactPerson },
                    { "Called", payOffData.Called },
                    { "NoteTime", DateTime.Now.ToString("MM/dd/yy h:mm tt") }
            };

            await _emailSender.SendEmailAsync(
            new MessageDto
            {
                TemplateId = (int)EmailTemplates.RequestPayOff,
                To = _configuration[GPFSettings.REQUESTPAYOFFEMAIL]
            }, null, placeHolders).ConfigureAwait(true);
            
            await Mediator.Send(new UpdateRequestPayOffCommand
            {
                PortfolioId = caseData.PortfolioId,
                UserName = UserName,
                Message = Message
            }).ConfigureAwait(true);
            return Ok(true);
        }
        [HttpGet]
        public async Task<object> GetFundedAmount(int CaseId)
        {
            var fundedAmounts = await Mediator.Send(new GetFundedAmountQuery() { CaseId = CaseId }).ConfigureAwait(true);
            return Json(fundedAmounts);
        }
        [HttpGet]
        public async Task<object> PayoffRequestReason(DataSourceLoadOptions loadOptions)
        {
            var caseDueUpdates = await Mediator.Send(new GetPayoffRequestReasonQuery() { }).ConfigureAwait(true);
            return DataSourceLoader.Load(caseDueUpdates, loadOptions);
        }
    }
}
