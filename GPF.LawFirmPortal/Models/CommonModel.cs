﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Models
{
    public class CaseUpdateKey
    {
        public int PortfolioId { get; set; }
        public int CaseId { get; set; }
    }
}
