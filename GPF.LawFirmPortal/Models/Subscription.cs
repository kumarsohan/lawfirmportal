﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Models
{

    public class Subscription
    {
        public string endpoint { get; set; }
        public string p256dh { get; set; }
        public string auth { get; set; }
    }
    public class MessageNotification
    {
        public string Note { get; set; }
    }
}
