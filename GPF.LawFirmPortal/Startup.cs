using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ElmahCore.Mvc;
using ElmahCore.Sql;
using Microsoft.AspNetCore.Http;
using ElmahCore.Mvc.Notifiers;
using GPF.LawFirmPortal.Application.Common.Utility;
using GPF.LawFirmPortal.Application;
using GPF.LawFirmPortal.Infrastructure;
using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Web.Services;
using GPF.LawFirmPortal.Web.Common;
using Microsoft.AspNetCore.Authentication.Cookies;
using Audit.SqlServer.Providers;
using Audit.SqlServer;
using Audit.Core;
using Audit.Mvc;
using GPF.LawFirmPortal.Hubs;

namespace GPF.LawFirmPortal
{
    public class Startup
    {
        public Startup(IConfiguration configuration,IWebHostEnvironment webHostEnvironment)
        {
            Configuration = configuration;
            Environment = webHostEnvironment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var MailRecipient = Configuration[AppConstants.EmailSettings.TOEMAIL];
            var SmtpServer = Configuration[AppConstants.EmailSettings.HOST];
            var MailSender = Configuration[AppConstants.EmailSettings.FROMEMAIL];
            var SmtpPort = Convert.ToInt32(Configuration[AppConstants.EmailSettings.PORT]);
            var UseSsl = Convert.ToBoolean(Configuration[AppConstants.EmailSettings.ENABLESSL]);

            services.AddElmah<SqlErrorLog>(options =>
            {
                options.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
                options.CheckPermissionAction = (context) =>
                {
                    //var userIdentity = new System.Security.Claims.ClaimsIdentity("Custom");
                    if (context == null) return false;
                    return true;// context.User.Identity.IsAuthenticated;
                };
                options.Path = "/LawFirmPortalLogs";
                options.Notifiers.Add(new ErrorMailNotifier("Email", new EmailOptions()
                {
                    MailRecipient = MailRecipient,
                    MailSender = MailSender,
                    MailSubjectFormat = "Law Firm Portal::DEV",
                    SendYsod = true,
                    SmtpPort = SmtpPort,
                    SmtpServer = SmtpServer,
                    UseSsl = UseSsl
                }));
                options.Filters.Add(new CmsErrorLogFilter());
            });
            services.AddMvc(mvc =>
            {
                mvc.EnableEndpointRouting = false;
            });


            //services.AddScoped<IUserClaimsPrincipalFactory<IdentityUser>, GPFClaimsPrincipalFactory>();
            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.Configure<Appsettings>(Configuration.GetSection("Appsettings"));
            services.AddHttpContextAccessor();
            services.AddApplication();
            services.AddInfrastructure(Configuration, Environment);            
            services.AddControllersWithViews().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddRazorPages();
            services.AddSignalR();
            //services.ConfigureApplicationCookie(options =>
            //{
            //    // Cookie settings
            //    options.Cookie.HttpOnly = true;
            //    options.LoginPath = "/Account/Login";
            //    options.AccessDeniedPath = "/Account/AccessDenied";
            //    options.Cookie.Name = CookieAuthenticationDefaults.AuthenticationScheme;
            //    options.SlidingExpiration = true;
            //});            

            services.AddAuthentication(
                CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                {
                    options.Cookie.Name = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.LoginPath = "/Account/Login";
                    options.LogoutPath = "/Account/Login";
                    options.AccessDeniedPath = "/Account/AccessDenied";
                    //options.SlidingExpiration = true;
                    //options.ReturnUrlParameter = "returnUrl";
                });
            services.AddAuthorization(options =>
            {
                options.AddPolicy("Require.LawFirmPortal.User.Auth", policy =>
                                  policy.RequireClaim("lawfirmportal.user.auth", "true")
                                  //.AddAuthenticationSchemes()
                                        .AddAuthenticationSchemes(CookieAuthenticationDefaults.AuthenticationScheme)
                                      );
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            #region Audit.net

            Audit.Core.Configuration.DataProvider = new SqlDataProvider()
            {               
                ConnectionString = this.Configuration.GetConnectionString("DefaultConnection"),
                Schema = "dbo",
                TableName = "EventAudit",
                IdColumnName = "EventId",
                JsonColumnName = "JsonData",
                LastUpdatedDateColumnName = "LastUpdatedDate",
                CustomColumns = new List<CustomColumn>()
                {
                    new CustomColumn("UserName", ev => ev.Environment.UserName),
                    new CustomColumn("EventType", ev => ev.EventType),
                    new CustomColumn("CreatedDate", ev=>DateTime.Now)
                }
            };

            var svcProvider = services.BuildServiceProvider();
            Audit.Core.Configuration.AddOnCreatedAction(scope =>
            {
                var mvcAction = scope.Event.GetMvcAuditAction();
                var httpContext = svcProvider.GetRequiredService<IHttpContextAccessor>().HttpContext;
                if (scope.EventType == "Login")
                {
                    var login = mvcAction.ActionParameters.FirstOrDefault(p => p.Key == "model").Value as Models.LoginViewModel;
                    if (login.UserName != null)
                    {
                        scope.SetCustomField("UserName", login.UserName);
                        scope.Event.Environment.CustomFields["HttpContextUsername"] = login.UserName;
                        scope.Event.Environment.UserName = login.UserName;
                    }
                }
                else
                {
                    scope.SetCustomField("UserName", ClaimsExtensions.GetClaimValue(httpContext.User, "Email"));
                    scope.Event.Environment.CustomFields["HttpContextUsername"] = ClaimsExtensions.GetClaimValue(httpContext.User, "Email");
                    scope.Event.Environment.UserName = ClaimsExtensions.GetClaimValue(httpContext.User, "Email");
                }
            });
            Audit.Core.Configuration.AddCustomAction(ActionType.OnScopeCreated, action =>
            {
                var mvcAction =action.Event.GetMvcAuditAction();
                if (mvcAction != null)
                {
                    if (mvcAction.ActionParameters != null && mvcAction.ActionParameters.ContainsKey("Password"))
                        mvcAction.ActionParameters["Password"] = "***";
                    if (mvcAction.FormVariables != null && mvcAction.FormVariables.ContainsKey("Password"))
                        mvcAction.FormVariables["Password"] = "***";
                }     
            });

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
           
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseElmah();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Dashboard}/{id?}");
                endpoints.MapHub<NotificationHub>("/NotificationHub");
                endpoints.MapRazorPages();
            });
            
        }
    }
    public class CmsErrorLogFilter : ElmahCore.IErrorFilter
    {
        public void OnErrorModuleFiltering(object sender, ElmahCore.ExceptionFilterEventArgs args)
        {
            if (args.Exception.GetBaseException() is System.IO.FileNotFoundException)
                args.Dismiss();
            if (args.Context is HttpContext httpContext)
                if (httpContext.Response.StatusCode == 404)
                    args.Dismiss();
        }
    }
}
