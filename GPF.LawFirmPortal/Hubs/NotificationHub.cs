﻿using GPF.LawFirmPortal.Application.Common.Interfaces;
using GPF.LawFirmPortal.Web.Common;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GPF.LawFirmPortal.Hubs
{
    [Authorize(Policy = "Require.LawFirmPortal.User.Auth", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    public class NotificationHub:Hub
    {
        private readonly IUserConnectionManager _userConnectionManager;
        public NotificationHub(IUserConnectionManager userConnectionManager)
        {
            _userConnectionManager = userConnectionManager;
        }
        public string GetConnectionId()
        {
            var user = ClaimsExtensions.GetClaimValue(Context.User, "UserName");
            _userConnectionManager.KeepUserConnection(user, Context.ConnectionId);

            return Context.ConnectionId;
        }
        public async override Task OnConnectedAsync()
        {
            var user = ClaimsExtensions.GetClaimValue(Context.User, "UserGroup");

            if (user != null)
            {
                if (user == "Developers" || user == "LawFirmPortal")
                {
                    await AddToGroup("NotificationGroup");
                }              
            }
            else
            {
                await Clients.Caller.SendAsync("ReceiveNotification", "Connection Error", 0);
            }
        }
        public async override Task OnDisconnectedAsync(Exception exception)
        {
            var connectionId = Context.ConnectionId;
            _userConnectionManager.RemoveUserConnection(connectionId);
            await RemoveFromGroup("NotificationGroup");
            var value = await Task.FromResult(0);//adding dump code to follow the template of Hub > OnDisconnectedAsync
        }

        public async Task SendNotification(string group, string message)
        {
            await Clients.OthersInGroup(group).SendAsync("ReceiveNotification", message);
        }
        
        public async Task AddToGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);          
        }

        public async Task RemoveFromGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }
    }
}
